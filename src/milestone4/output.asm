; [0, 'proc_label', 'f2', None, None]
; [1, 'A', 4, ':=', None]
; [2, 'proc_label', 'f3', 4, None]
; [3, 'C', 4, ':=', None]
; [4, 'return', ' c', None, None]
; [5, '_t0', 'A', '<', 'b']
; [6, 'd', '_t0', ':=', None]
; [7, 'I', 1, ':=', None]
; [8, 'goto', 10, None, None]
; [9, 'I', 'I', 'int_+', 1]
; [10, 'blteq', 'I', 10, 12]
; [11, 'goto', 22, None, None]
; [12, '_t1', 'I', 'int_+', 1]
; [13, 'A', '_t1', ':=', None]
; [14, '_t2', 'I', 'int_*', 2]
; [15, '_t3', 'A', 'int_+', '_t2']
; [16, 'b', '_t3', ':=', None]
; [17, 'param', 'C', 'OUT', None]
; [18, 'param', 'B', 'IN', None]
; [19, 'param', 'A', 'IN', None]
; [20, 'call_proced', 'f3', 3, None]
; [21, 'goto', 9, None, None]
; [22, 'return', None, None, None]
;setting up .data section
;============================
;           x86 code
;============================
section .text
	global _start

_P_f3:
	push ebp
	mov ebp, esp
	mov eax, 4
	mov [c], eax
	push  c
	pop ebp
	ret

_start:
	mov eax, 4
	mov [a], eax
	mov eax, [a]
	cmp eax, b
	jl _L5
	xor eax, eax
	mov [_t0], eax
	jmp _L6
_L5:
	xor eax, eax
	inc eax
	mov [_t0], eax
_L6:
	mov eax, _t0
	mov [d], eax
	mov eax, 1
	mov [i], eax
	jmp _L1
_L4:
	mov eax, i
	add eax, 1
	mov [i], eax
_L1:
	mov eax, 10
	cmp [i], eax
	jle _L2
	jmp _L3
_L2:
	mov eax, i
	add eax, 1
	mov [_t1], eax
	mov eax, _t1
	mov [a], eax
	mov eax, i
	mov ebx, 2
	mul ebx
	mov [_t2], eax
	mov eax, a
	add eax, _t2
	mov [_t3], eax
	mov eax, _t3
	mov [b], eax
	push c	;param c out
	push b	;param b in
	push a	;param a in
	call _P_f3	;call f3
	jmp _L4
_L3:
	mov eax, 1
	xor ebx, ebx
	int 0x80

section .bss
	a resb 4
	c resb 4
	b resb 4
	d resb 4
	_t0 resb 4
	_t1 resb 4
	_t2 resb 4
	_t3 resb 4
	i resb 4

;============================
;Parsing successful for file test_file
