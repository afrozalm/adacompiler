from elements import Element


class CodeGenerator(object):
    def __init__(self, symbol_table, TACode_list):
        self.symbol_table = symbol_table
        self.TACode_list = TACode_list
        self.asm_code = ''
        self.code = ''
        self.bss_vars = []
        self.labeled_lines = []
        self.label_ref = {}
        self.labelno = 0
        self.proced_lists = {}
        self.code_stack = {}
        self.current_section = None
        table = symbol_table.hash_table
        valid = False
        if len(table) == 1:
            valid = True
        if valid:
            for key in table:
                driver_fn = table[key]
                self.driver_fn_name = key
                self.driver_table = driver_fn.hash_table
        else:
            print 'length of hash_table is', len(table)
            self.driver_table = {}

    def get_next_label(self):
        self.labelno += 1
        return '_L' + str(self.labelno)

    def setup_labels(self):
        for instr in self.TACode_list:
            lineno = instr[0]
            if instr[1] == 'goto':
                self.labeled_lines += [lineno]
                target_line = instr[2]
                self.label_ref[target_line] = self.get_next_label()
            elif instr[1] in ['blteq', 'bgteq']:
                self.labeled_lines += [lineno]
                target_line = instr[4]
                self.label_ref[target_line] = self.get_next_label()
            elif instr[1] == 'if':
                target_line = instr[4]
                self.label_ref[target_line] = self.get_next_label()

    def int_arithmetic(self, lhs, op1, op, op2):
        if op[4] in ['+', '-']:
            if '+' == op[4]:
                opcode = '\tadd '
            elif '-' == op[4]:
                opcode = '\tsub '
            self.code += '\tmov eax, ' + op1 + '\n' \
                         + opcode + 'eax, ' + op2 + '\n'
            self.move_reg_to_var(lhs, 'eax')
        else:
            if '*' == op[4]:
                self.code += '\tmov eax, ' + op1 + '\n' \
                             + '\tmov ebx, ' + op2 + '\n' \
                             + '\tmul ebx\n'
                self.move_reg_to_var(lhs, 'eax')
            elif '/' == op[4]:
                self.code += '\tmov eax, ' + op1 + '\n' \
                             + '\tmov ebx, ' + op2 + '\n' \
                             + '\txor edx, edx\n' \
                             + '\tdiv ebx\n'
                self.move_reg_to_var(lhs, 'eax')

    def float_arithmetic(self, lhs, op1, op, op2):
        if op[6] in ['+', '-']:
            if '+' == op[6]:
                opcode = '\tfadd '
            elif '-' == op[6]:
                opcode = '\tfsub '
            self.code += '\tmov eax, ' + op1 + '\n' \
                         + opcode + 'eax, ' + op2 + '\n'
            self.move_reg_to_var(lhs, 'eax')
        else:
            if '*' == op[6]:
                self.code += '\tmov eax, ' + op1 + '\n' \
                             + '\tmov ebx, ' + op2 + '\n' \
                             + '\tfmul ebx\n'
                self.move_reg_to_var(lhs, 'eax')
            elif '/' == op[6]:
                self.code += '\tmov eax, ' + op1 + '\n' \
                             + '\tmov ebx, ' + op2 + '\n' \
                             + '\txor edx, edx\n' \
                             + '\tfdiv ebx\n'
                self.move_reg_to_var(lhs, 'eax')

    def handle_branch_jumps(self, lhs, op1, op, op2):
        jmp_to = self.label_ref[int(op2)]
        if lhs == 'blteq':
            jmp_stmt = '\tjle ' + jmp_to + '\n'
        elif lhs == 'bgteq':
            jmp_stmt = '\tjge ' + jmp_to + '\n'

        self.code += '\tmov eax, ' + op + '\n' \
                     + '\tcmp [' + op1 + '], ' + 'eax\n'\
                     + jmp_stmt

    def unary_arithmetic(self, lhs, op, op2):
        if '+' == op[-1]:
            self.assign_val(lhs, op2, 'eax')
        else:
            self.code += '\tmov eax, ' + op2 + '\n' \
                         + '\tneg eax\n'
            self.move_reg_to_var(lhs, 'eax')

    def handle_bool(self, lhs, op1, op, op2):
        # lhs will be zero if cond(op1, op, op2) is false
        if op == '<':
            jmp_stmt = '\tjl '
        elif op == '<=':
            jmp_stmt = '\tjle '
        elif op == '>':
            jmp_stmt = '\tjg '
        elif op == '>=':
            jmp_stmt = '\tjge '
        elif op == '=':
            jmp_stmt = '\tje '
        else:
            jmp_stmt = 'jne '
        true_lbl = self.get_next_label()
        next_lbl = self.get_next_label()
        self.move_var_to_reg(op1, 'eax')
        self.code += '\tcmp eax, ' + op2 + '\n' \
                     + jmp_stmt + true_lbl + '\n' \
                     + '\txor eax, eax\n'
        self.move_reg_to_var(lhs, 'eax')
        self.code += '\tjmp ' + next_lbl + '\n' \
                     + true_lbl + ':\n' \
                     + '\txor eax, eax\n' \
                     + '\tinc eax\n'
        self.move_reg_to_var(lhs, 'eax')
        self.code += next_lbl + ':\n'

    def handle_if(self, lhs, op1, op, op2):
        true_lbl = self.label_ref[int(op2)]
        self.move_var_to_reg(op1, 'eax')
        self.code += '\txor ebx, ebx\n' \
                     + '\tcmp eax, ebx\n' \
                     + '\tjnz ' + true_lbl + '\n'

    def handle_proc(self, op1):
        proced_name = '_P_' + op1
        if op1 != self.driver_fn_name:
            self.code_stack[self.current_section[-1]] += self.code
            self.code_stack[proced_name] = ''
            self.code = ''
            self.current_section += [proced_name]
            self.code += "_P_" + op1 + ':\n' \
                         + '\tpush ebp\n' \
                         + '\tmov ebp, esp\n'

    def handle_param(self, name, mode):
        self.code += '\tpush ' + name + '\t;param ' \
                     + name + ' ' + mode + '\n'

    def handle_call(self, p_label):
        self.code += '\tcall _P_' + p_label + '\t;call ' \
                     + p_label + '\n'

    def handle_return(self, arg):
        # this is wrong; we need to make changes in the address of arg
        # ----------------------------
        if arg != 'none':
            self.code += '\tpush ' + arg + '\n'
        # ----------------------------
        if self.current_section[-1] != '_start':
            self.code += '\tpop ebp\n' \
                         + '\tret\n'
        self.code_stack[self.current_section[-1]] += self.code
        self.code = ''
        self.current_section.pop()

    def setup_text(self):
        self.code_stack['_start'] = ''
        self.code += '_start:\n'
        self.current_section = ['_start']
        for instr in self.TACode_list:
            lineno = instr[0]
            lhs = str(instr[1]).lower()
            op1 = str(instr[2]).lower()
            op = str(instr[3]).lower()
            op2 = str(instr[4]).lower()
            if lineno in self.label_ref:
                self.code += self.label_ref[lineno] + ':\n'

            if 'unary_' in op:
                self.unary_arithmetic(lhs, op, op2)
            elif 'int_' in op:
                self.int_arithmetic(lhs, op1, op, op2)
            elif 'float_' in op:
                self.float_arithmetic(lhs, op1, op, op2)
            elif op in ['<', '<=', '>', '>=', '=', '/=']:
                self.handle_bool(lhs, op1, op, op2)
            elif op == ':=':
                self.assign_val(lhs, op1, 'eax')
                # self.code += '\tmov ' + lhs + ', ' + op1 + '\n'
            elif lhs == 'goto':
                self.code += '\tjmp ' + self.label_ref[int(op1)] + '\n'
            elif lhs in ['blteq', 'bgteq']:
                self.handle_branch_jumps(lhs, op1, op, op2)
            elif lhs == 'if':
                self.handle_if(lhs, op1, op, op2)
            elif lhs == 'proc_label':
                self.handle_proc(op1)
            elif lhs == 'param':
                self.handle_param(op1, op)
            elif lhs == 'call_proced':
                self.handle_call(op1)
            elif lhs == 'return':
                self.handle_return(op1)

    def setup_bss(self):
        self.asm_code += 'section .bss\n'
        for name, element in self.driver_table.iteritems():
            if isinstance(element, Element):
                width = element.Type.width
                self.asm_code += '\t' + name + ' resb ' + str(width) + '\n'
                self.bss_vars += [name]

    def setup_data(self):
        print ';setting up .data section'

    def setup_exit(self):
        self.code_stack['_start'] += '\tmov eax, 1\n' \
                                     + '\txor ebx, ebx\n' \
                                     + '\tint 0x80\n'

    def setup_all(self):
        self.setup_labels()
        self.asm_code += 'section .text\n\tglobal _start\n\n'
        self.setup_text()
        self.setup_exit()
        for proced_name, code in self.code_stack.iteritems():
            if proced_name != '_start':
                self.asm_code += code + '\n'
        self.asm_code += self.code_stack['_start']
        self.asm_code += '\n'
        self.setup_data()
        self.setup_bss()

    def display_code(self):
        print ';============================'
        print ';           x86 code'
        print ';============================'
        print self.asm_code
        print ';============================'

    def assign_val(self, var, val, reg):
        self.code += '\tmov ' + reg + ', ' + val + '\n'\
                     + '\tmov [' + var + '], ' + reg + '\n'

    def move_reg_to_var(self, var, reg):
        self.code += '\tmov [' + var + '], ' + reg + '\n'

    def move_var_to_reg(self, var, reg):
        self.code += '\tmov ' + reg + ', [' + var + ']\n'
