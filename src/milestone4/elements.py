from copy import deepcopy

basic_types = ['INTEGER', 'FLOAT', 'CHAR',
               'BOOLEAN', 'NULL', 'ERROR']

complex_types = ['ARRAY', 'FUNCTION', 'PROCEDURE', 'STATEMENT']

width_dict = {'INTEGER': 4, 'FLOAT': 4, 'CHAR': 1,
              'BOOLEAN': 4, 'NULL': 0, 'ERROR': 0}


class Element(object):
    def __init__(self, Name=None, Type=None, Value=None,
                 Mode=None, Place=None, Offset=None,
                 truelist=None, falselist=None,
                 quad=None, nextlist=None):
        self.Name = Name
        self.Type = Type
        self.Value = Value
        self.Mode = Mode  # ['IN', 'OUT', 'IO', 'ACCESS']
        self.Offset = Offset
        self.truelist = truelist
        self.falselist = falselist
        self.quad = quad
        self.nextlist = nextlist

    def operate(self, operator, operants, p):
        '''
        operants : a list of elements on which the operator acts.

        returns true is successful
                false is error and prints error
        '''
        err_msg_prefix = 'Error detected at line : ' + str(p.lineno) + ' '
        if operator == '*':
            if len(operants) == 2:
                op1, op2 = operants
                op1_name = op1.Type.type_name
                op2_name = op2.Type.type_name
                out_op1 = op1.Name if op1.Name is not None else op1.Value
                out_op2 = op2.Name if op2.Name is not None else op2.Value
                valid = ['INTEGER', 'FLOAT']
                if op1_name in valid and op2_name in valid:
                    if op1_name == op2_name:
                        if op1_name == 'INTEGER':
                            self.Type = Type(type_name='INTEGER')
                            return [None, out_op1, 'int_*', out_op2]
                        else:
                            self.Type = Type(type_name='FLOAT')
                            return [None, out_op1, 'flt_*', out_op2]
                    else:
                        print err_msg_prefix \
                            + 'Type mismatch error'
                        return None
            else:
                print err_msg_prefix \
                    + '* operator accepts only two arguments of float/int type'
                self.Type = Type(type_name='ERROR')
                return None

        elif operator == '/':
            if len(operants) == 2:
                valid = ['INTEGER', 'FLOAT']
                op1, op2 = operants
                op1_name = op1.Type.type_name
                op2_name = op2.Type.type_name
                out_op1 = op1.Name if op1.Name is not None else op1.Value
                out_op2 = op2.Name if op2.Name is not None else op2.Value
                if op1_name in valid and op2_name in valid:
                    if op2.Value == 0:
                        self.Type = Type(type_name='ERROR')
                        print 'Divide by zero error detected'
                    else:
                        if op1_name == op2_name:
                            if op1_name == 'INTEGER':
                                self.Type = Type(type_name='INTEGER')
                                return [None, out_op1, 'int_/', out_op2]
                            else:
                                self.Type = Type(type_name='FLOAT')
                                return [None, out_op1, 'flt_/', out_op2]
                        else:
                            print err_msg_prefix \
                                + 'Type mismatch error'
                            return None
            else:
                print err_msg_prefix \
                    + '/ operator accepts only two arguments of float/int type'
                self.Type = Type(type_name='ERROR')
                return None

        elif operator in ['+', '-']:
            if len(operants) == 2:
                op1, op2 = operants
                op1_name = op1.Type.type_name
                op2_name = op2.Type.type_name
                out_op1 = op1.Name if op1.Name is not None else op1.Value
                out_op2 = op2.Name if op2.Name is not None else op2.Value
                valid = ['INTEGER', 'FLOAT']
                if op1_name in valid and op2_name in valid:
                    if op1_name == op2_name:
                        if op1_name == 'INTEGER':
                            self.Type = Type(type_name='INTEGER')
                            return [None, out_op1, 'int_' + operator, out_op2]
                        else:
                            self.Type = Type(type_name='FLOAT')
                            return [None, out_op1, 'flt_' + operator, out_op2]
                    else:
                        print err_msg_prefix \
                            + 'Type mismatch error'
                        return None
            elif len(operants) == 1:
                op1 = operants[0]
                out_op1 = op1.Value if op1.Value is not None else op1.Name
                if op1.Type.type_name in ['INTEGER', 'FLOAT']:
                    self = deepcopy(operator[0])
                    return [None, None, 'unary_' + operator, out_op1]
                else:
                    self.Type = Type(type_name='ERROR')
                    print err_msg_prefix + operator \
                        + ' operator accepts argument of type float/integer'
                    return None
            else:
                print err_msg_prefix + operator \
                    + ' operator accepts argument of type float/integer'
                self.Type = Type(type_name='ERROR')
                return None

        elif operator.upper() in ['REM', 'MOD']:
            op1, op2 = operants
            op1_name = op1.Type.type_name
            op2_name = op2.Type.type_name
            out_op1 = op1.Name if op1.Name is not None else op1.Value
            out_op2 = op2.Name if op2.Name is not None else op2.Value
            if op1_name == 'INTEGER' and op2_name == 'INTEGER':
                self.Type = Type(type_name='INTEGER')
                return [None, out_op1, 'MOD', out_op2]
            else:
                print err_msg_prefix \
                    + operator + ' operator accepts arguments of type integer'
                self.Type = Type(type_name='ERROR')
                return None

        elif operator in ['<', '<=', '>', '>=', '=', '/=']:
            if len(operants) == 2:
                op1, op2 = operants
                op1_name = op1.Type.type_name
                op2_name = op2.Type.type_name
                out_op1 = op1.Name if op1.Name is not None else op1.Value
                out_op2 = op2.Name if op2.Name is not None else op2.Value
                cond1 = op1_name in ['INTEGER', 'FLOAT']
                cond2 = op2_name in ['INTEGER', 'FLOAT']
                cond5 = cond1 and cond2

                if cond5:
                    self.Type = Type(type_name='BOOLEAN')
                    return [None, out_op1, operator,  out_op2]
                else:
                    print err_msg_prefix \
                        + operator \
                        + ' operates on two arguments of int/float'
            else:
                print err_msg_prefix \
                    + operator \
                    + ' operates on two arguments of int/float'

        else:
            print 'operator ' + operator + ' not handled'
            return False

    def get_attrs(self, prefix=''):
        output = prefix
        if self.Name is not None:
            output += self.Name
        else:
            output += '<not assigned>'

        if self.Type is not None:
            if self.Type.type_name in basic_types:
                output += ', ' + self.Type.type_name
            elif self.Type.type_name == 'ARRAY':
                output += ', array of ' + self.Type.constituent_type \
                          + ' with dimensionality ' + str(self.Type.dim)
            elif self.Type.type_name is not None:
                output += ', ' + self.Type.type_name
            else:
                output += ', <not assigned>'
        else:
            output += ', <not assigned>'

        if self.Offset is not None:
            output += ', ' + str(self.Offset)
        else:
            output += ', <not assigned>'

        if self.Value is not None:
            if self.Type.type_name in basic_types:
                output += ', ' + str(self.Value)
            elif self.Type.type_name == 'ARRAY' and self.Type.constituent_type == 'CHAR':
                output += ', ' + self.Value
            elif self.Type.type_name == 'ARRAY' and not self.Type.constituent_type == 'CHAR':
                output += ', <non char array cannot be displayed>'
            else:
                output += ', <cannot display complex types except char array>'
        else:
            output += ', <not assigned>'

        return output


class Type(object):
    def __init__(self, type_name=None,
                 low=None, high=None, size=None, constituent_type=None,
                 dim=None, dim_ranges=[], is_range_set=False,
                 is_const=False, is_aliased=False,
                 params=None, width=None):
        self.type_name = type_name
        '''is_const is used for identifiers which as statically defined'''
        self.is_const = is_const
        self.is_aliased = is_aliased
        '''dim_ranges is a list of tuples used for storing allowed ranges in
        different dimesions'''
        self.dim_ranges = dim_ranges
        '''the dimensionality of array'''
        self.dim = dim
        '''Total size of the array = d0 x d1 x d2 x....'''
        self.size = size
        '''high, low and is_range_set are used for ranges'''
        self.high = high
        self.low = low
        self.is_range_set = is_range_set
        '''constituent_type is used for array type'''
        self.constituent_type = constituent_type
        '''for procedure types holding list of elements
        with name value type moade set'''
        self.params = params
        self.width = width

    def get_width(self):
        if self.width is not None:
            return self.width
        elif self.type_name is not None:
            if self.type_name in basic_types:
                self.width = width_dict[self.type_name]
                return self.width
            elif self.type_name == 'ARRAY':
                constituent_width = width_dict[self.constituent_type]
                size = 1
                for t in self.dim_ranges:
                    low_idx, high_idx = t
                    size *= high_idx - low_idx + 1
                self.width = size*constituent_width
                return self.width
            else:
                return None
        else:
            return None

    def assign_width(self, width=None):
        if width is not None:
            self.width = width
        elif self.type_name is not None:
            if self.type_name in basic_types:
                self.width = width_dict[self.type_name]
                return self.width
            elif self.type_name == 'ARRAY':
                constituent_width = width_dict[self.constituent_type]
                size = 1
                for t in self.dim_ranges:
                    low_idx, high_idx = t
                    size *= high_idx - low_idx + 1
                self.width = size*constituent_width
                return self.width
            else:
                return None
        else:
            return None
