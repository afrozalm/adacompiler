#!/usr/bin/env python

import ply.yacc as yacc
import pydot
from grammar import Rules
from symbol_table import SymbolTable
from three_add_code import ThreeAddCode

pydot_error_chars = ',#:@'


class Ada_Parser(Rules):
    def __init__(self, debug=False):
        self.debug = debug
        self.symb_output = ''
        self.symbol_table = SymbolTable(parent_table=None)
        from token_file import build_lexer
        self.tokens, self.lexer = build_lexer(debug_mode=False)
        self.parser = yacc.yacc(module=self)

        self.three_add_code = ThreeAddCode(self.symbol_table)
        self.successful = True
        self.graph = pydot.Dot(graph_type='digraph', bgcolor='#1e5e68')
        self.nnodes = 0

    def process(self, filename):
        assert type(filename) is str
        try:
            with open(filename, 'rb') as fp:
                data = fp.read()
                self.parser.parse(data)
                if self.successful:
                    print ';Parsing successful for file ' + filename
                else:
                    print 'Parse for ' + filename + ' failed'
        except IOError:
            print "Unable to find " + filename


if __name__ == '__main__':
    import sys
    # p1 = Ada_Parser(debug=True)
    p1 = Ada_Parser()
    assert len(sys.argv) > 1
    for i in xrange(1, len(sys.argv)):
        p1.successful = True
        p1.process(sys.argv[i])
