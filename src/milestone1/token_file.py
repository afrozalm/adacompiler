'''
This file contains the definitions of tokens required
to tokenize ada files.
'''
import ply.lex as lex
def build_lexer(debug_mode=True, optimize_mode=False):
    # regular expression definitions taken from https://github.com/ayusek/ada-compiler/blob/master/Lexer/adaTokens.py

    from reserved_tokens import reserved

    tokens = [
        'IDENTIFIER',
        'STRING',
        'CHAR',
        'INTEGER',
        'BASE_INTEGER',
        'FLOAT',
        'BASE_FLOAT',

        'ARROW',      #=>
        'DOUBLESTAR', #STARSTAR **
        'DOUBLEDOT',  #DOTDOT    ..
        'ASSIGNMENT', # :=
        'NOTEQUAL',   # /=
        'GEQ',        #GREATEREQ >=
        'LEQ',        #LESSEQ   <=
        'LEFTLABEL',  #LESSLESS  <<
        'RIGHTLABEL', #MOREMORE  >>
        'BOX',        #LESSMORE <>
        'TICK'        # '
    ] + list( reserved.values())

    literals = "&()*+,-./:;<=>"

    # t_token = value
    t_ARROW = r'=>'
    t_DOUBLESTAR = r'\*\*'
    t_DOUBLEDOT = r'\.\.'
    t_ASSIGNMENT = r':='
    t_NOTEQUAL = r'/='
    t_GEQ = r'>='
    t_LEQ = r'<='
    t_LEFTLABEL = r'<<'
    t_RIGHTLABEL = r'>>'
    t_BOX = r'<>'

    def t_IDENTIFIER(t):
        r'[A-Za-z](_?[A-Za-z0-9])*'
        t.type = reserved.get(t.value.lower(), 'IDENTIFIER')         # will return reserved if it matches
        return t

    def t_TICK(t):
        r'\''
        return t

    def t_CHAR(t):
        r'\'.\''   # to match input of the form 'A'
        return t

    def t_STRING(t):
        r'\"((\"\")|[^"])*\"' #Strings start and end with " only
        return t

    def t_BASE_FLOAT(t):
        r'(0*[0-9]|1[0-6])\#((([0-9A-F](_?[0-9A-F]+)*(\.[0-9A-F](_?[0-9A-F]+)*)?)\#[eE]\-[0-9](_?[0-9]+)*)|([0-9A-F](_?[0-9A-F]+)*\.[0-9A-F](_?[0-9A-F]+)*)\#([eE][\+]?[0-9](_?[0-9]+)*)?)'
        #Exponents are in decimal base only.
        t.value = t.value.replace("_", "")
        return t

    def t_BASE_INTEGER(t):
        r'(0*[2-9]|1[0-6])\#[0-9A-F](_?[0-9A-F]+)*\#([Ee](\+)?[0-9A-F](_?[0-9A-F]+)*)?'
        t.value = t.value.replace("_", "")
        return t

    def t_INTEGER(t):
        r'[0-9](_?[0-9]+)*([Ee](\+)?[0-9](_?[0-9]+)*)?'
        t.value = int(float(t.value.replace("_","")))
        return t

    def t_FLOAT(t):
        r'(([0-9](_?[0-9]+)*(\.[0-9](_?[0-9]+)*)?)[eE]\-[0-9](_?[0-9]+)*)|([0-9](_?[0-9]+)*\.[0-9](_?[0-9]+)*)([eE][\+]?[0-9](_?[0-9]+)*)?'
        #single character literals used as Operators or Delimiters
        t.value = float(t.value.replace("_",""))
        return t

    '''
    The following rule are taken form ply tutorial for easier debugging.
    source : http://www.dabeaz.com/ply/ply.html#ply_nn4
    '''
    # Define a rule so we can track line numbers
    def t_newline(t):
        r'\n+'
        t.lexer.lineno += len(t.value)

    # A string containing ignored characters (spaces and tabs)
    t_ignore  = ' \t'
    t_ignore_COMMENT = r'--.*'
    # Error handling rule
    def t_error(t):
        print("Illegal character '%s'" % t.value[0])
        t.lexer.skip(1)

    # Build the lexer
    if debug_mode:
        return [tokens, lex.lex(debug=1)]
    elif optimize_mode:
        return [tokens, lex.lex(optimize=1)]    # disables error checking
    else:
        return [tokens, lex.lex()]
