class Rules(object):
        def p_start_symbol(self, p ):
                '''start_symbol : compilation
                '''
                p[0] = self.makenode(p, 'start_symbol')
        def p_pragma(self, p):
                '''pragma  : PRAGMA IDENTIFIER ';'
               | PRAGMA simple_name '(' pragma_arg_s ')' ';'
            '''
                p[0] = self.makenode(p, 'pragma ')

        def p_pragma_arg_s(self, p):
                '''pragma_arg_s : pragma_arg
               | pragma_arg_s ',' pragma_arg
            '''
                p[0] = self.makenode(p, 'pragma_arg_s')

        def p_pragma_arg(self, p):
                '''pragma_arg : expression
               | simple_name ARROW expression
            '''
                p[0] = self.makenode(p, 'pragma_arg')

        def p_pragma_s(self, p):
                '''pragma_s :
               | pragma_s pragma
            '''
                p[0] = self.makenode(p, 'pragma_s')

        def p_decl(self, p):
                '''decl    : object_decl
               | number_decl
               | type_decl
               | subtype_decl
               | subprog_decl
               | pkg_decl
               | task_decl
               | prot_decl
               | exception_decl
               | rename_decl
               | generic_decl
               | body_stub
               | error ';'
            '''
                p[0] = self.makenode(p, 'decl   ')

        def p_object_decl(self, p):
                '''object_decl : def_id_s ':' object_qualifier_opt object_subtype_def init_opt ';'
            '''
                p[0] = self.makenode(p, 'object_decl')

        def p_def_id_s(self, p):
                '''def_id_s : def_id
               | def_id_s ',' def_id
            '''
                p[0] = self.makenode(p, 'def_id_s')

        def p_def_id(self, p):
                '''def_id  : IDENTIFIER
            '''
                p[0] = self.makenode(p, 'def_id ')

        def p_object_qualifier_opt(self, p):
                '''object_qualifier_opt :
               | ALIASED
               | CONSTANT
               | ALIASED CONSTANT
            '''
                p[0] = self.makenode(p, 'object_qualifier_opt')

        def p_object_subtype_def(self, p):
                '''object_subtype_def : subtype_ind
               | array_type
            '''
                p[0] = self.makenode(p, 'object_subtype_def')

        def p_init_opt(self, p):
                '''init_opt :
               | ASSIGNMENT expression
            '''
                p[0] = self.makenode(p, 'init_opt')

        def p_number_decl(self, p):
                '''number_decl : def_id_s ':' CONSTANT ASSIGNMENT expression ';'
            '''
                p[0] = self.makenode(p, 'number_decl')

        def p_type_decl(self, p):
                '''type_decl : TYPE IDENTIFIER discrim_part_opt type_completion ';'
            '''
                p[0] = self.makenode(p, 'type_decl')

        def p_discrim_part_opt(self, p):
                '''discrim_part_opt :
               | discrim_part
               | '(' BOX ')'
            '''
                p[0] = self.makenode(p, 'discrim_part_opt')

        def p_type_completion(self, p):
                '''type_completion :
               | IS type_def
            '''
                p[0] = self.makenode(p, 'type_completion')

        def p_type_def(self, p):
                '''type_def : enumeration_type
               | integer_type
               | real_type
               | array_type
               | record_type
               | access_type
               | derived_type
               | private_type
            '''
                p[0] = self.makenode(p, 'type_def')

        def p_subtype_decl(self, p):
                '''subtype_decl : SUBTYPE IDENTIFIER IS subtype_ind ';'
            '''
                p[0] = self.makenode(p, 'subtype_decl')

        def p_subtype_ind(self, p):
                '''subtype_ind : name constraint
               | name
            '''
                p[0] = self.makenode(p, 'subtype_ind')

        def p_constraint(self, p):
                '''constraint : range_constraint
               | decimal_digits_constraint
            '''
                p[0] = self.makenode(p, 'constraint')

        def p_decimal_digits_constraint(self, p):
                '''decimal_digits_constraint : DIGITS expression range_constr_opt
            '''
                p[0] = self.makenode(p, 'decimal_digits_constraint')

        def p_derived_type(self, p):
                '''derived_type : NEW subtype_ind
               | NEW subtype_ind WITH PRIVATE
               | NEW subtype_ind WITH record_def
               | ABSTRACT NEW subtype_ind WITH PRIVATE
               | ABSTRACT NEW subtype_ind WITH record_def
            '''
                p[0] = self.makenode(p, 'derived_type')

        def p_range_constraint(self, p):
                '''range_constraint : RANGE range
            '''
                p[0] = self.makenode(p, 'range_constraint')

        def p_range(self, p):
                '''range : simple_expression DOUBLEDOT simple_expression
               | name TICK RANGE
               | name TICK RANGE '(' expression ')'
            '''
                p[0] = self.makenode(p, 'range')

        def p_enumeration_type(self, p):
                '''enumeration_type : '(' enum_id_s ')'
            '''
                p[0] = self.makenode(p, 'enumeration_type')

        def p_enum_id_s(self, p):
                '''enum_id_s : enum_id
               | enum_id_s ',' enum_id
            '''
                p[0] = self.makenode(p, 'enum_id_s')

        def p_enum_id(self, p):
                '''enum_id : IDENTIFIER
               | CHAR
            '''
                p[0] = self.makenode(p, 'enum_id')

        def p_integer_type(self, p):
                '''integer_type : range_spec
               | MOD expression
            '''
                p[0] = self.makenode(p, 'integer_type')

        def p_range_spec(self, p):
                '''range_spec : range_constraint
            '''
                p[0] = self.makenode(p, 'range_spec')

        def p_range_spec_opt(self, p):
                '''range_spec_opt :
               | range_spec
            '''
                p[0] = self.makenode(p, 'range_spec_opt')

        def p_real_type(self, p):
                '''real_type : float_type
               | fixed_type
            '''
                p[0] = self.makenode(p, 'real_type')

        def p_float_type(self, p):
                '''float_type : DIGITS expression range_spec_opt
            '''
                p[0] = self.makenode(p, 'float_type')

        def p_fixed_type(self, p):
                '''fixed_type : DELTA expression range_spec
               | DELTA expression DIGITS expression range_spec_opt
            '''
                p[0] = self.makenode(p, 'fixed_type')

        def p_array_type(self, p):
                '''array_type : unconstr_array_type
               | constr_array_type
            '''
                p[0] = self.makenode(p, 'array_type')

        def p_unconstr_array_type(self, p):
                '''unconstr_array_type : ARRAY '(' index_s ')' OF component_subtype_def
            '''
                p[0] = self.makenode(p, 'unconstr_array_type')

        def p_constr_array_type(self, p):
                '''constr_array_type : ARRAY iter_index_constraint OF component_subtype_def
            '''
                p[0] = self.makenode(p, 'constr_array_type')

        def p_component_subtype_def(self, p):
                '''component_subtype_def : aliased_opt subtype_ind
            '''
                p[0] = self.makenode(p, 'component_subtype_def')

        def p_aliased_opt(self, p):
                '''aliased_opt :
               | ALIASED
            '''
                p[0] = self.makenode(p, 'aliased_opt')

        def p_index_s(self, p):
                '''index_s : index
               | index_s ',' index
            '''
                p[0] = self.makenode(p, 'index_s')

        def p_index(self, p):
                '''index : name RANGE BOX
            '''
                p[0] = self.makenode(p, 'index')

        def p_iter_index_constraint(self, p):
                '''iter_index_constraint : '(' iter_discrete_range_s ')'
            '''
                p[0] = self.makenode(p, 'iter_index_constraint')

        def p_iter_discrete_range_s(self, p):
                '''iter_discrete_range_s : discrete_range
               | iter_discrete_range_s ',' discrete_range
            '''
                p[0] = self.makenode(p, 'iter_discrete_range_s')

        def p_discrete_range(self, p):
                '''discrete_range : name range_constr_opt
               | range
            '''
                p[0] = self.makenode(p, 'discrete_range')

        def p_range_constr_opt(self, p):
                '''range_constr_opt :
               | range_constraint
            '''
                p[0] = self.makenode(p, 'range_constr_opt')

        def p_record_type(self, p):
                '''record_type : tagged_opt limited_opt record_def
            '''
                p[0] = self.makenode(p, 'record_type')

        def p_record_def(self, p):
                '''record_def : RECORD pragma_s comp_list END RECORD
               | NULL RECORD
            '''
                p[0] = self.makenode(p, 'record_def')

        def p_tagged_opt(self, p):
                '''tagged_opt :
               | TAGGED
               | ABSTRACT TAGGED
            '''
                p[0] = self.makenode(p, 'tagged_opt')

        def p_comp_list(self, p):
                '''comp_list : comp_decl_s variant_part_opt
               | variant_part pragma_s
               | NULL ';' pragma_s
            '''
                p[0] = self.makenode(p, 'comp_list')

        def p_comp_decl_s(self, p):
                '''comp_decl_s : comp_decl
               | comp_decl_s pragma_s comp_decl
            '''
                p[0] = self.makenode(p, 'comp_decl_s')

        def p_variant_part_opt(self, p):
                '''variant_part_opt : pragma_s
               | pragma_s variant_part pragma_s
            '''
                p[0] = self.makenode(p, 'variant_part_opt')

        def p_comp_decl(self, p):
                '''comp_decl : def_id_s ':' component_subtype_def init_opt ';'
               | error ';'
            '''
                p[0] = self.makenode(p, 'comp_decl')

        def p_discrim_part(self, p):
                '''discrim_part : '(' discrim_spec_s ')'
            '''
                p[0] = self.makenode(p, 'discrim_part')

        def p_discrim_spec_s(self, p):
                '''discrim_spec_s : discrim_spec
               | discrim_spec_s ';' discrim_spec
            '''
                p[0] = self.makenode(p, 'discrim_spec_s')

        def p_discrim_spec(self, p):
                '''discrim_spec : def_id_s ':' access_opt mark init_opt
               | error
            '''
                p[0] = self.makenode(p, 'discrim_spec')

        def p_access_opt(self, p):
                '''access_opt :
               | ACCESS
            '''
                p[0] = self.makenode(p, 'access_opt')

        def p_variant_part(self, p):
                '''variant_part : CASE simple_name IS pragma_s variant_s END CASE ';'
            '''
                p[0] = self.makenode(p, 'variant_part')

        def p_variant_s(self, p):
                '''variant_s : variant
               | variant_s variant
            '''
                p[0] = self.makenode(p, 'variant_s')

        def p_variant(self, p):
                '''variant : WHEN choice_s ARROW pragma_s comp_list
            '''
                p[0] = self.makenode(p, 'variant')

        def p_choice_s(self, p):
                '''choice_s : choice
               | choice_s '|' choice
            '''
                p[0] = self.makenode(p, 'choice_s')

        def p_choice(self, p):
                '''choice : expression
               | discrete_with_range
               | OTHERS
            '''
                p[0] = self.makenode(p, 'choice')

        def p_discrete_with_range(self, p):
                '''discrete_with_range : name range_constraint
               | range
            '''
                p[0] = self.makenode(p, 'discrete_with_range')

        def p_access_type(self, p):
                '''access_type : ACCESS subtype_ind
               | ACCESS CONSTANT subtype_ind
               | ACCESS ALL subtype_ind
               | ACCESS prot_opt PROCEDURE formal_part_opt
               | ACCESS prot_opt FUNCTION formal_part_opt RETURN mark
            '''
                p[0] = self.makenode(p, 'access_type')

        def p_prot_opt(self, p):
                '''prot_opt :
               | PROTECTED
            '''
                p[0] = self.makenode(p, 'prot_opt')

        def p_decl_part(self, p):
                '''decl_part :
               | decl_item_or_body_s1
            '''
                p[0] = self.makenode(p, 'decl_part')

        def p_decl_item_s(self, p):
                '''decl_item_s :
               | decl_item_s1
            '''
                p[0] = self.makenode(p, 'decl_item_s')

        def p_decl_item_s1(self, p):
                '''decl_item_s1 : decl_item
               | decl_item_s1 decl_item
            '''
                p[0] = self.makenode(p, 'decl_item_s1')

        def p_decl_item(self, p):
                '''decl_item : decl
               | use_clause
               | rep_spec
               | pragma
            '''
                p[0] = self.makenode(p, 'decl_item')

        def p_decl_item_or_body_s(self, p):
                '''decl_item_or_body_s1 : decl_item_or_body
               | decl_item_or_body_s1 decl_item_or_body
            '''
                p[0] = self.makenode(p, 'decl_item_or_body_s1')

        def p_decl_item_or_body(self, p):
                '''decl_item_or_body : body
               | decl_item
            '''
                p[0] = self.makenode(p, 'decl_item_or_body')

        def p_body(self, p):
                '''body : subprog_body
               | pkg_body
               | task_body
               | prot_body
            '''
                p[0] = self.makenode(p, 'body')

        def p_name(self, p):
                '''name : simple_name
               | indexed_comp
               | selected_comp
               | attribute
               | operator_symbol
            '''
                p[0] = self.makenode(p, 'name')

        def p_mark(self, p):
                '''mark : simple_name
               | mark TICK attribute_id
               | mark '.' simple_name
            '''
                p[0] = self.makenode(p, 'mark')

        def p_simple_name(self, p):
                '''simple_name : IDENTIFIER
            '''
                p[0] = self.makenode(p, 'simple_name')

        def p_compound_name(self, p):
                '''compound_name : simple_name
               | compound_name '.' simple_name
            '''
                p[0] = self.makenode(p, 'compound_name')

        def p_c_name_list(self, p):
                '''c_name_list : compound_name
                | c_name_list ',' compound_name
            '''
                p[0] = self.makenode(p, 'c_name_list')

        def p_used_char(self, p):
                '''used_char : CHAR
            '''
                p[0] = self.makenode(p, 'used_char')

        def p_operator_symbol(self, p):
                '''operator_symbol : STRING
            '''
                p[0] = self.makenode(p, 'operator_symbol')

        def p_indexed_comp(self, p):
                '''indexed_comp : name '(' value_s ')'
            '''
                p[0] = self.makenode(p, 'indexed_comp')

        def p_value_s(self, p):
                '''value_s : value
               | value_s ',' value
            '''
                p[0] = self.makenode(p, 'value_s')

        def p_value(self, p):
                '''value : expression
               | comp_assoc
               | discrete_with_range
               | error
            '''
                p[0] = self.makenode(p, 'value')

        def p_selected_comp(self, p):
                '''selected_comp : name '.' simple_name
               | name '.' used_char
               | name '.' operator_symbol
               | name '.' ALL
            '''
                p[0] = self.makenode(p, 'selected_comp')

        def p_attribute(self, p):
                '''attribute : name TICK attribute_id
            '''
                p[0] = self.makenode(p, 'attribute')

        def p_attribute_id(self, p):
                '''attribute_id : IDENTIFIER
               | DIGITS
               | DELTA
               | ACCESS
            '''
                p[0] = self.makenode(p, 'attribute_id')

        def p_literal(self, p):
                '''literal : INTEGER
               | BASE_INTEGER
               | FLOAT
               | BASE_FLOAT
               | used_char
               | NULL
            '''
                p[0] = self.makenode(p, 'literal')

        def p_aggregate(self, p):
                '''aggregate : '(' comp_assoc ')'
               | '(' value_s_2 ')'
               | '(' expression WITH value_s ')'
               | '(' expression WITH NULL RECORD ')'
               | '(' NULL RECORD ')'
            '''
                p[0] = self.makenode(p, 'aggregate')

        def p_value_s_(self, p):
                '''value_s_2 : value ',' value
               | value_s_2 ',' value
            '''
                p[0] = self.makenode(p, 'value_s_2')

        def p_comp_assoc(self, p):
                '''comp_assoc : choice_s ARROW expression
            '''
                p[0] = self.makenode(p, 'comp_assoc')

        def p_expression(self, p):
                '''expression : relation
               | expression logical relation
               | expression short_circuit relation
            '''
                p[0] = self.makenode(p, 'expression')

        def p_logical(self, p):
                '''logical : AND
               | OR
               | XOR
            '''
                p[0] = self.makenode(p, 'logical')

        def p_short_circuit(self, p):
                '''short_circuit : AND THEN
               | OR ELSE
            '''
                p[0] = self.makenode(p, 'short_circuit')

        def p_relation(self, p):
                '''relation : simple_expression
               | simple_expression relational simple_expression
               | simple_expression membership range
               | simple_expression membership name
            '''
                p[0] = self.makenode(p, 'relation')

        def p_relational(self, p):
                '''relational : '='
               | NOTEQUAL
               | '<'
               | LEQ
               | '>'
               | GEQ
            '''
                p[0] = self.makenode(p, 'relational')

        def p_membership(self, p):
                '''membership : IN
               | NOT IN
            '''
                p[0] = self.makenode(p, 'membership')

        def p_simple_expression(self, p):
                '''simple_expression : unary term
               | term
               | simple_expression adding term
            '''
                p[0] = self.makenode(p, 'simple_expression')

        def p_unary(self, p):
                '''unary   : '+'
               | '-'
            '''
                p[0] = self.makenode(p, 'unary  ')

        def p_adding(self, p):
                '''adding  : '+'
               | '-'
               | '&'
            '''
                p[0] = self.makenode(p, 'adding ')

        def p_term(self, p):
                '''term    : factor
               | term multiplying factor
            '''
                p[0] = self.makenode(p, 'term   ')

        def p_multiplying(self, p):
                '''multiplying : '*'
               | '/'
               | MOD
               | REM
            '''
                p[0] = self.makenode(p, 'multiplying')

        def p_factor(self, p):
                '''factor : primary
               | NOT primary
               | ABS primary
               | primary DOUBLESTAR primary
            '''
                p[0] = self.makenode(p, 'factor')

        def p_primary(self, p):
                '''primary : literal
               | name
               | allocator
               | qualified
               | parenthesized_primary
            '''
                p[0] = self.makenode(p, 'primary')

        def p_parenthesized_primary(self, p):
                '''parenthesized_primary : aggregate
               | '(' expression ')'
            '''
                p[0] = self.makenode(p, 'parenthesized_primary')

        def p_qualified(self, p):
                '''qualified : name TICK parenthesized_primary
            '''
                p[0] = self.makenode(p, 'qualified')

        def p_allocator(self, p):
                '''allocator : NEW name
               | NEW qualified
            '''
                p[0] = self.makenode(p, 'allocator')

        def p_statement_s(self, p):
                '''statement_s : statement
               | statement_s statement
            '''
                p[0] = self.makenode(p, 'statement_s')

        def p_statement(self, p):
                '''statement : unlabeled
               | label statement
            '''
                p[0] = self.makenode(p, 'statement')

        def p_unlabeled(self, p):
                '''unlabeled : simple_stmt
               | compound_stmt
               | pragma
            '''
                p[0] = self.makenode(p, 'unlabeled')

        def p_simple_stmt(self, p):
                '''simple_stmt : null_stmt
               | assign_stmt
               | exit_stmt
               | return_stmt
               | goto_stmt
               | procedure_call
               | delay_stmt
               | abort_stmt
               | raise_stmt
               | code_stmt
               | requeue_stmt
               | error ';'
            '''
                p[0] = self.makenode(p, 'simple_stmt')

        def p_compound_stmt(self, p):
                '''compound_stmt : if_stmt
               | case_stmt
               | loop_stmt
               | block
               | accept_stmt
               | select_stmt
            '''
                p[0] = self.makenode(p, 'compound_stmt')

        def p_label(self, p):
                '''label : LEFTLABEL IDENTIFIER RIGHTLABEL
            '''
                p[0] = self.makenode(p, 'label')

        def p_null_stmt(self, p):
                '''null_stmt : NULL ';'
            '''
                p[0] = self.makenode(p, 'null_stmt')

        def p_assign_stmt(self, p):
                '''assign_stmt : name ASSIGNMENT expression ';'
            '''
                p[0] = self.makenode(p, 'assign_stmt')

        def p_if_stmt(self, p):
                '''if_stmt : IF cond_clause_s else_opt END IF ';'
            '''
                p[0] = self.makenode(p, 'if_stmt')

        def p_cond_clause_s(self, p):
                '''cond_clause_s : cond_clause
               | cond_clause_s ELSIF cond_clause
            '''
                p[0] = self.makenode(p, 'cond_clause_s')

        def p_cond_clause(self, p):
                '''cond_clause : cond_part statement_s
            '''
                p[0] = self.makenode(p, 'cond_clause')

        def p_cond_part(self, p):
                '''cond_part : condition THEN
            '''
                p[0] = self.makenode(p, 'cond_part')

        def p_condition(self, p):
                '''condition : expression
            '''
                p[0] = self.makenode(p, 'condition')

        def p_else_opt(self, p):
                '''else_opt :
               | ELSE statement_s
            '''
                p[0] = self.makenode(p, 'else_opt')

        def p_case_stmt(self, p):
                '''case_stmt : case_hdr pragma_s alternative_s END CASE ';'
            '''
                p[0] = self.makenode(p, 'case_stmt')

        def p_case_hdr(self, p):
                '''case_hdr : CASE expression IS
            '''
                p[0] = self.makenode(p, 'case_hdr')

        def p_alternative_s(self, p):
                '''alternative_s :
               | alternative_s alternative
            '''
                p[0] = self.makenode(p, 'alternative_s')

        def p_alternative(self, p):
                '''alternative : WHEN choice_s ARROW statement_s
            '''
                p[0] = self.makenode(p, 'alternative')

        def p_loop_stmt(self, p):
                '''loop_stmt : label_opt iteration basic_loop id_opt ';'
            '''
                p[0] = self.makenode(p, 'loop_stmt')

        def p_label_opt(self, p):
                '''label_opt :
               | IDENTIFIER ':'
            '''
                p[0] = self.makenode(p, 'label_opt')

        def p_iteration(self, p):
                '''iteration :
               | WHILE condition
               | iter_part reverse_opt discrete_range
            '''
                p[0] = self.makenode(p, 'iteration')

        def p_iter_part(self, p):
                '''iter_part : FOR IDENTIFIER IN
            '''
                p[0] = self.makenode(p, 'iter_part')

        def p_reverse_opt(self, p):
                '''reverse_opt :
               | REVERSE
            '''
                p[0] = self.makenode(p, 'reverse_opt')

        def p_basic_loop(self, p):
                '''basic_loop : LOOP statement_s END LOOP
            '''
                p[0] = self.makenode(p, 'basic_loop')

        def p_id_opt(self, p):
                '''id_opt :
               | designator
            '''
                p[0] = self.makenode(p, 'id_opt')

        def p_block(self, p):
                '''block : label_opt block_decl block_body END id_opt ';'
            '''
                p[0] = self.makenode(p, 'block')

        def p_block_decl(self, p):
                '''block_decl :
               | DECLARE decl_part
            '''
                p[0] = self.makenode(p, 'block_decl')

        def p_block_body(self, p):
                '''block_body : BEGIN handled_stmt_s
            '''
                p[0] = self.makenode(p, 'block_body')

        def p_handled_stmt_s(self, p):
                '''handled_stmt_s : statement_s except_handler_part_opt
            '''
                p[0] = self.makenode(p, 'handled_stmt_s')

        def p_except_handler_part_opt(self, p):
                '''except_handler_part_opt :
               | except_handler_part
            '''
                p[0] = self.makenode(p, 'except_handler_part_opt')

        def p_exit_stmt(self, p):
                '''exit_stmt : EXIT name_opt when_opt ';'
            '''
                p[0] = self.makenode(p, 'exit_stmt')

        def p_name_opt(self, p):
                '''name_opt :
               | name
            '''
                p[0] = self.makenode(p, 'name_opt')

        def p_when_opt(self, p):
                '''when_opt :
               | WHEN condition
            '''
                p[0] = self.makenode(p, 'when_opt')

        def p_return_stmt(self, p):
                '''return_stmt : RETURN ';'
               | RETURN expression ';'
            '''
                p[0] = self.makenode(p, 'return_stmt')

        def p_goto_stmt(self, p):
                '''goto_stmt : GOTO name ';'
            '''
                p[0] = self.makenode(p, 'goto_stmt')

        def p_subprog_decl(self, p):
                '''subprog_decl : subprog_spec ';'
               | generic_subp_inst ';'
               | subprog_spec_is_push ABSTRACT ';'
            '''
                p[0] = self.makenode(p, 'subprog_decl')

        def p_subprog_spec(self, p):
                '''subprog_spec : PROCEDURE compound_name formal_part_opt
               | FUNCTION designator formal_part_opt RETURN name
               | FUNCTION designator
            '''
                p[0] = self.makenode(p, 'subprog_spec')

        def p_designator(self, p):
                '''designator : compound_name
               | STRING
            '''
                p[0] = self.makenode(p, 'designator')

        def p_formal_part_opt(self, p):
                '''formal_part_opt :
               | formal_part
            '''
                p[0] = self.makenode(p, 'formal_part_opt')

        def p_formal_part(self, p):
                '''formal_part : '(' param_s ')'
            '''
                p[0] = self.makenode(p, 'formal_part')

        def p_param_s(self, p):
                '''param_s : param
               | param_s ';' param
            '''
                p[0] = self.makenode(p, 'param_s')

        def p_param(self, p):
                '''param : def_id_s ':' mode mark init_opt
               | error
            '''
                p[0] = self.makenode(p, 'param')

        def p_mode(self, p):
                '''mode :
               | IN
               | OUT
               | IN OUT
               | ACCESS
            '''
                p[0] = self.makenode(p, 'mode')

        def p_subprog_spec_is_push(self, p):
                '''subprog_spec_is_push : subprog_spec IS
            '''
                p[0] = self.makenode(p, 'subprog_spec_is_push')

        def p_subprog_body(self, p):
                '''subprog_body : subprog_spec_is_push decl_part block_body END id_opt ';'
            '''
                p[0] = self.makenode(p, 'subprog_body')

        def p_procedure_call(self, p):
                '''procedure_call : name ';'
            '''
                p[0] = self.makenode(p, 'procedure_call')

        def p_pkg_decl(self, p):
                '''pkg_decl : pkg_spec ';'
               | generic_pkg_inst ';'
            '''
                p[0] = self.makenode(p, 'pkg_decl')

        def p_pkg_spec(self, p):
                '''pkg_spec : PACKAGE compound_name IS decl_item_s private_part END c_id_opt
            '''
                p[0] = self.makenode(p, 'pkg_spec')

        def p_private_part(self, p):
                '''private_part :
               | PRIVATE decl_item_s
            '''
                p[0] = self.makenode(p, 'private_part')

        def p_c_id_opt(self, p):
                '''c_id_opt :
               | compound_name
            '''
                p[0] = self.makenode(p, 'c_id_opt')

        def p_pkg_body(self, p):
                '''pkg_body : PACKAGE BODY compound_name IS decl_part body_opt END c_id_opt ';'
            '''
                p[0] = self.makenode(p, 'pkg_body')

        def p_body_opt(self, p):
                '''body_opt :
               | block_body
            '''
                p[0] = self.makenode(p, 'body_opt')

        def p_private_type(self, p):
                '''private_type : tagged_opt limited_opt PRIVATE
            '''
                p[0] = self.makenode(p, 'private_type')

        def p_limited_opt(self, p):
                '''limited_opt :
               | LIMITED
            '''
                p[0] = self.makenode(p, 'limited_opt')

        def p_use_clause(self, p):
                '''use_clause : USE name_s ';'
               | USE TYPE name_s ';'
            '''
                p[0] = self.makenode(p, 'use_clause')

        def p_name_s(self, p):
                '''name_s : name
               | name_s ',' name
            '''
                p[0] = self.makenode(p, 'name_s')

        def p_rename_decl(self, p):
                '''rename_decl : def_id_s ':' object_qualifier_opt subtype_ind renames ';'
               | def_id_s ':' EXCEPTION renames ';'
               | rename_unit
            '''
                p[0] = self.makenode(p, 'rename_decl')

        def p_rename_unit(self, p):
                '''rename_unit : PACKAGE compound_name renames ';'
               | subprog_spec renames ';'
               | generic_formal_part PACKAGE compound_name renames ';'
               | generic_formal_part subprog_spec renames ';'
            '''
                p[0] = self.makenode(p, 'rename_unit')

        def p_renames(self, p):
                '''renames : RENAMES name
            '''
                p[0] = self.makenode(p, 'renames')

        def p_task_decl(self, p):
                '''task_decl : task_spec ';'
            '''
                p[0] = self.makenode(p, 'task_decl')

        def p_task_spec(self, p):
                '''task_spec : TASK simple_name task_def
               | TASK TYPE simple_name discrim_part_opt task_def
            '''
                p[0] = self.makenode(p, 'task_spec')

        def p_task_def(self, p):
                '''task_def :
               | IS entry_decl_s rep_spec_s task_private_opt END id_opt
            '''
                p[0] = self.makenode(p, 'task_def')

        def p_task_private_opt(self, p):
                '''task_private_opt :
               | PRIVATE entry_decl_s rep_spec_s
            '''
                p[0] = self.makenode(p, 'task_private_opt')

        def p_task_body(self, p):
                '''task_body : TASK BODY simple_name IS decl_part block_body END id_opt ';'
            '''
                p[0] = self.makenode(p, 'task_body')

        def p_prot_decl(self, p):
                '''prot_decl : prot_spec ';'
            '''
                p[0] = self.makenode(p, 'prot_decl')

        def p_prot_spec(self, p):
                '''prot_spec : PROTECTED IDENTIFIER prot_def
               | PROTECTED TYPE simple_name discrim_part_opt prot_def
            '''
                p[0] = self.makenode(p, 'prot_spec')

        def p_prot_def(self, p):
                '''prot_def : IS prot_op_decl_s prot_private_opt END id_opt
            '''
                p[0] = self.makenode(p, 'prot_def')

        def p_prot_private_opt(self, p):
                '''prot_private_opt :
               | PRIVATE prot_elem_decl_s
            '''
                p[0] = self.makenode(p, 'prot_private_opt')

        def p_prot_op_decl_s(self, p):
                '''prot_op_decl_s :
               | prot_op_decl_s prot_op_decl
            '''
                p[0] = self.makenode(p, 'prot_op_decl_s')

        def p_prot_op_decl(self, p):
                '''prot_op_decl : entry_decl
               | subprog_spec ';'
               | rep_spec
               | pragma
            '''
                p[0] = self.makenode(p, 'prot_op_decl')

        def p_prot_elem_decl_s(self, p):
                '''prot_elem_decl_s :
               | prot_elem_decl_s prot_elem_decl
            '''
                p[0] = self.makenode(p, 'prot_elem_decl_s')

        def p_prot_elem_decl(self, p):
                '''prot_elem_decl : prot_op_decl
            | comp_decl
            '''
                p[0] = self.makenode(p, 'prot_elem_decl')

        def p_prot_body(self, p):
                '''prot_body : PROTECTED BODY simple_name IS prot_op_body_s END id_opt ';'
            '''
                p[0] = self.makenode(p, 'prot_body')

        def p_prot_op_body_s(self, p):
                '''prot_op_body_s : pragma_s
               | prot_op_body_s prot_op_body pragma_s
            '''
                p[0] = self.makenode(p, 'prot_op_body_s')

        def p_prot_op_body(self, p):
                '''prot_op_body : entry_body
               | subprog_body
               | subprog_spec ';'
            '''
                p[0] = self.makenode(p, 'prot_op_body')

        def p_entry_decl_s(self, p):
                '''entry_decl_s : pragma_s
               | entry_decl_s entry_decl pragma_s
            '''
                p[0] = self.makenode(p, 'entry_decl_s')

        def p_entry_decl(self, p):
                '''entry_decl : ENTRY IDENTIFIER formal_part_opt ';'
               | ENTRY IDENTIFIER '(' discrete_range ')' formal_part_opt ';'
            '''
                p[0] = self.makenode(p, 'entry_decl')

        def p_entry_body(self, p):
                '''entry_body : ENTRY IDENTIFIER formal_part_opt WHEN condition entry_body_part
               | ENTRY IDENTIFIER '(' iter_part discrete_range ')' formal_part_opt WHEN condition entry_body_part
            '''
                p[0] = self.makenode(p, 'entry_body')

        def p_entry_body_part(self, p):
                '''entry_body_part : ';'
               | IS decl_part block_body END id_opt ';'
            '''
                p[0] = self.makenode(p, 'entry_body_part')

        def p_rep_spec_s(self, p):
                '''rep_spec_s :
               | rep_spec_s rep_spec pragma_s
            '''
                p[0] = self.makenode(p, 'rep_spec_s')

        def p_entry_call(self, p):
                '''entry_call : procedure_call
            '''
                p[0] = self.makenode(p, 'entry_call')

        def p_accept_stmt(self, p):
                '''accept_stmt : accept_hdr ';'
               | accept_hdr DO handled_stmt_s END id_opt ';'
            '''
                p[0] = self.makenode(p, 'accept_stmt')

        def p_accept_hdr(self, p):
                '''accept_hdr : ACCEPT entry_name formal_part_opt
            '''
                p[0] = self.makenode(p, 'accept_hdr')

        def p_entry_name(self, p):
                '''entry_name : simple_name
               | entry_name '(' expression ')'
            '''
                p[0] = self.makenode(p, 'entry_name')

        def p_delay_stmt(self, p):
                '''delay_stmt : DELAY expression ';'
               | DELAY UNTIL expression ';'
            '''
                p[0] = self.makenode(p, 'delay_stmt')

        def p_select_stmt(self, p):
                '''select_stmt : select_wait
               | async_select
               | timed_entry_call
               | cond_entry_call
            '''
                p[0] = self.makenode(p, 'select_stmt')

        def p_select_wait(self, p):
                '''select_wait : SELECT guarded_select_alt or_select else_opt  END SELECT ';'
            '''
                p[0] = self.makenode(p, 'select_wait')

        def p_guarded_select_alt(self, p):
                '''guarded_select_alt : select_alt
               | WHEN condition ARROW select_alt
            '''
                p[0] = self.makenode(p, 'guarded_select_alt')

        def p_or_select(self, p):
                '''or_select :
               | or_select OR guarded_select_alt
            '''
                p[0] = self.makenode(p, 'or_select')

        def p_select_alt(self, p):
                '''select_alt : accept_stmt stmts_opt
               | delay_stmt stmts_opt
               | TERMINATE ';'
            '''
                p[0] = self.makenode(p, 'select_alt')

        def p_delay_or_entry_alt(self, p):
                '''delay_or_entry_alt : delay_stmt stmts_opt
               | entry_call stmts_opt
            '''
                p[0] = self.makenode(p, 'delay_or_entry_alt')

        def p_async_select(self, p):
                '''async_select : SELECT delay_or_entry_alt THEN ABORT statement_s END SELECT ';'
            '''
                p[0] = self.makenode(p, 'async_select')

        def p_timed_entry_call(self, p):
                '''timed_entry_call : SELECT entry_call stmts_opt OR delay_stmt stmts_opt END SELECT ';'
            '''
                p[0] = self.makenode(p, 'timed_entry_call')

        def p_cond_entry_call(self, p):
                '''cond_entry_call : SELECT entry_call stmts_opt ELSE statement_s END SELECT ';'
            '''
                p[0] = self.makenode(p, 'cond_entry_call')

        def p_stmts_opt(self, p):
                '''stmts_opt :
               | statement_s
            '''
                p[0] = self.makenode(p, 'stmts_opt')

        def p_abort_stmt(self, p):
                '''abort_stmt : ABORT name_s ';'
            '''
                p[0] = self.makenode(p, 'abort_stmt')

        def p_compilation(self, p):
                '''compilation :
               | compilation comp_unit
               | pragma pragma_s
            '''
                p[0] = self.makenode(p, 'compilation')

        def p_comp_unit(self, p):
                '''comp_unit : context_spec private_opt unit pragma_s
               | private_opt unit pragma_s
            '''
                p[0] = self.makenode(p, 'comp_unit')

        def p_private_opt(self, p):
                '''private_opt :
               | PRIVATE
            '''
                p[0] = self.makenode(p, 'private_opt')

        def p_context_spec(self, p):
                '''context_spec : with_clause use_clause_opt
               | context_spec with_clause use_clause_opt
               | context_spec pragma
            '''
                p[0] = self.makenode(p, 'context_spec')

        def p_with_clause(self, p):
                '''with_clause : WITH c_name_list ';'
            '''
                p[0] = self.makenode(p, 'with_clause')

        def p_use_clause_opt(self, p):
                '''use_clause_opt :
               | use_clause_opt use_clause
            '''
                p[0] = self.makenode(p, 'use_clause_opt')

        def p_unit(self, p):
                '''unit : pkg_decl
               | pkg_body
               | subprog_decl
               | subprog_body
               | subunit
               | generic_decl
               | rename_unit
            '''
                p[0] = self.makenode(p, 'unit')

        def p_subunit(self, p):
                '''subunit : SEPARATE '(' compound_name ')' subunit_body
            '''
                p[0] = self.makenode(p, 'subunit')

        def p_subunit_body(self, p):
                '''subunit_body : subprog_body
               | pkg_body
               | task_body
               | prot_body
            '''
                p[0] = self.makenode(p, 'subunit_body')

        def p_body_stub(self, p):
                '''body_stub : TASK BODY simple_name IS SEPARATE ';'
               | PACKAGE BODY compound_name IS SEPARATE ';'
               | subprog_spec IS SEPARATE ';'
               | PROTECTED BODY simple_name IS SEPARATE ';'
            '''
                p[0] = self.makenode(p, 'body_stub')

        def p_exception_decl(self, p):
                '''exception_decl : def_id_s ':' EXCEPTION ';'
            '''
                p[0] = self.makenode(p, 'exception_decl')

        def p_except_handler_part(self, p):
                '''except_handler_part : EXCEPTION exception_handler
               | except_handler_part exception_handler
            '''
                p[0] = self.makenode(p, 'except_handler_part')

        def p_exception_handler(self, p):
                '''exception_handler : WHEN except_choice_s ARROW statement_s
               | WHEN IDENTIFIER ':' except_choice_s ARROW statement_s
            '''
                p[0] = self.makenode(p, 'exception_handler')

        def p_except_choice_s(self, p):
                '''except_choice_s : except_choice
               | except_choice_s '|' except_choice
            '''
                p[0] = self.makenode(p, 'except_choice_s')

        def p_except_choice(self, p):
                '''except_choice : name
               | OTHERS
            '''
                p[0] = self.makenode(p, 'except_choice')

        def p_raise_stmt(self, p):
                '''raise_stmt : RAISE name_opt ';'
            '''
                p[0] = self.makenode(p, 'raise_stmt')

        def p_requeue_stmt(self, p):
                '''requeue_stmt : REQUEUE name ';'
               | REQUEUE name WITH ABORT ';'
            '''
                p[0] = self.makenode(p, 'requeue_stmt')

        def p_generic_decl(self, p):
                '''generic_decl : generic_formal_part subprog_spec ';'
               | generic_formal_part pkg_spec ';'
            '''
                p[0] = self.makenode(p, 'generic_decl')

        def p_generic_formal_part(self, p):
                '''generic_formal_part : GENERIC
               | generic_formal_part generic_formal
            '''
                p[0] = self.makenode(p, 'generic_formal_part')

        def p_generic_formal(self, p):
                '''generic_formal : param ';'
               | TYPE simple_name generic_discrim_part_opt IS generic_type_def ';'
               | WITH PROCEDURE simple_name formal_part_opt subp_default ';'
               | WITH FUNCTION designator formal_part_opt RETURN name subp_default ';'
               | WITH PACKAGE simple_name IS NEW name '(' BOX ')' ';'
               | WITH PACKAGE simple_name IS NEW name ';'
               | use_clause
            '''
                p[0] = self.makenode(p, 'generic_formal')

        def p_generic_discrim_part_opt(self, p):
                '''generic_discrim_part_opt :
               | discrim_part
               | '(' BOX ')'
            '''
                p[0] = self.makenode(p, 'generic_discrim_part_opt')

        def p_subp_default(self, p):
                '''subp_default :
               | IS name
               | IS BOX
            '''
                p[0] = self.makenode(p, 'subp_default')

        def p_generic_type_def(self, p):
                '''generic_type_def : '(' BOX ')'
               | RANGE BOX
               | MOD BOX
               | DELTA BOX
               | DELTA BOX DIGITS BOX
               | DIGITS BOX
               | array_type
               | access_type
               | private_type
               | generic_derived_type
            '''
                p[0] = self.makenode(p, 'generic_type_def')

        def p_generic_derived_type(self, p):
                '''generic_derived_type : NEW subtype_ind
               | NEW subtype_ind WITH PRIVATE
               | ABSTRACT NEW subtype_ind WITH PRIVATE
            '''
                p[0] = self.makenode(p, 'generic_derived_type')

        def p_generic_subp_inst(self, p):
                '''generic_subp_inst : subprog_spec IS generic_inst
            '''
                p[0] = self.makenode(p, 'generic_subp_inst')

        def p_generic_pkg_inst(self, p):
                '''generic_pkg_inst : PACKAGE compound_name IS generic_inst
            '''
                p[0] = self.makenode(p, 'generic_pkg_inst')

        def p_generic_inst(self, p):
                '''generic_inst : NEW name
            '''
                p[0] = self.makenode(p, 'generic_inst')

        def p_rep_spec(self, p):
                '''rep_spec : attrib_def
               | record_type_spec
               | address_spec
            '''
                p[0] = self.makenode(p, 'rep_spec')

        def p_attrib_def(self, p):
                '''attrib_def : FOR mark USE expression ';'
            '''
                p[0] = self.makenode(p, 'attrib_def')

        def p_record_type_spec(self, p):
                '''record_type_spec : FOR mark USE RECORD align_opt comp_loc_s END RECORD ';'
            '''
                p[0] = self.makenode(p, 'record_type_spec')

        def p_align_opt(self, p):
                '''align_opt :
               | AT MOD expression ';'
            '''
                p[0] = self.makenode(p, 'align_opt')

        def p_comp_loc_s(self, p):
                '''comp_loc_s :
               | comp_loc_s mark AT expression RANGE range ';'
            '''
                p[0] = self.makenode(p, 'comp_loc_s')

        def p_address_spec(self, p):
                '''address_spec : FOR mark USE AT expression ';'
            '''
                p[0] = self.makenode(p, 'address_spec')

        def p_code_stmt(self, p):
                '''code_stmt : qualified ';'
            '''
                p[0] = self.makenode(p, 'code_stmt')

        def p_error(self, p):
                try:
                        self.successful = False
                        print "Error "+ str(p.type) + " found at line " + str(p.lineno) + " at position " + str( p.lexpos)
                        parser.errok()
                except:
                        print 'Unknown error'
