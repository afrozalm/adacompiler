#!/usr/bin/env python

import ply.yacc as yacc
import ply.lex as lex
import pydot
from grammar import Rules

pydot_error_chars = ',#:@'

class Ada_Parser(Rules):
    def __init__(self):
        from token_file import build_lexer
        self.tokens, self.lexer = build_lexer(debug_mode=False)
        self.parser = yacc.yacc(module=self)

        self.successful = True
        self.graph = pydot.Dot(graph_type='digraph', bgcolor='#1e5e68')
        self.nnodes = 0

    def makenode(self, p, nodeLabels):
        '''
        p : stack
        returns root node
        '''
        assert p[0]==None
        parent_name = self.get_unique_id()
        parent = pydot.Node(parent_name, label=nodeLabels, style="filled", fillcolor="white")
        self.graph.add_node(parent)

        for child in xrange(1,len(p)):
            if 'ADA_NODE' in str(p[child]):
                self.graph.add_edge(pydot.Edge(parent_name, p[child], color='yellow' ))
            else:
                child_name = self.get_unique_id()
                child_label = '\'"' + str(p[child]) +'"\''
                node_child = pydot.Node(child_name, label=child_label, style="filled",
                                        fillcolor="orange")
                self.graph.add_node(node_child)
                edge = pydot.Edge(parent_name,child_name, color="yellow")
                self.graph.add_edge(edge)
        return parent_name

    def plot_parse_tree(self, filename):
        # self.graph.write_dot(filename + '.dot')
        self.graph.write_ps(filename + '.ps')
        print 'Number of nodes created =', self.nnodes

    def get_unique_id(self):
        self.nnodes += 1
        return 'ADA_NODE' + str(self.nnodes)

    def process(self, filename):
        assert type(filename) is str
        try:
            with open(filename, 'rb') as fp:
                data = fp.read()
                self.parser.parse(data)
                if self.successful:
                    print 'Parsing successful for file ' + filename
                    self.plot_parse_tree(filename)
                else:
                    print 'Could not parser ' + filename + ' successfully'
        except IOError:
            print "Unable to find " + filename

if __name__ == '__main__':
    import sys
    p1 = Ada_Parser()
    assert len(sys.argv) > 1
    for i in xrange(1, len(sys.argv)):
        p1.successful = True
        p1.process(sys.argv[i])
