from reserved_tokens import reserved


class SymbolTable(object):
    def __init__(self, parent_table):
        self.parent_table = parent_table
        self.hash_table = {}

    def addtype(self, name, Attr_list, lineno):
        # check whether the entry is already persent in the symbol table
        if name in self.hash_table:
            # error
            print "Error in line " + str(lineno) +\
                ':' + name + " declared twice in scope"
        else:
            self.hash_table[name] = Attr_list

    def add_to_table(self, name, element, lineno):
        name = name.lower()
        if name in reserved:
            print name + ' is a reserved keyword'
            return False
        if not self.is_present_in_scope(name):
            self.hash_table[name] = element
            return True
        else:
            print 'Error in line ' + str(lineno)\
                + ' : ' + name + ' declared twice in scope'
            return False

    def update_table(self, name, element, lineno):
        print 'updating table'
        err_msg_prefix = 'Error in line ' + str(lineno)
        current_table = self
        if name in reserved:
            print err_msg_prefix + name \
                + ' is reserved keyword and cannot be assigned a value'
            return False
        while current_table is not None:
            # check if name is present in current_table table
            if name in current_table.hash_table:
                current_table.hash_table[name] = element
                return True
            current_table = current_table.parent_table
        print err_msg_prefix + name \
            + ' could not be found in the current scope'
        return False

    def lookup(self, name):
        name = name.lower()
        current_table = self
        while current_table is not None:
            if name in current_table.hash_table:
                return current_table.hash_table[name]
            current_table = current_table.parent_table
        return None

    def is_present_in_scope(self, name):
        if name in reserved:
            print name + ' is a reserved keyword'
            return True
        current_table = self
        while current_table is not None:
            if name in current_table.hash_table:
                return True
            current_table = current_table.parent_table
        return False

    def is_same_procedure(self, param_set_1, param_set_2):
        param_names = map(lambda x: x.Name, param_set_1)
        for p in param_set_2:
            if p.Name not in param_names:
                return False
        return True

    def setup_procedure(self, params, lineno):
        flag = True
        if params is not None:
            for param in params:
                name = param.Name.lower()
                if name not in reserved:
                    self.hash_table[param.Name.lower()] = param
                else:
                    print name + ' is a reserved type'
                    flag = False
        return flag

    def setup_function(self, params, lineno):
        flag = True
        if params is not None:
            for param in params:
                name = param.Name.lower()
                if name not in reserved:
                    self.hash_table[param.Name.lower()] = param
                else:
                    print name + ' is a reserved type'
                    flag = False
        return flag

    def add_symb_output(self):
        out_table = ''
        for name, element in self.hash_table.iteritems():
            out_table += element.get_attrs() + '\n'
        return out_table

    def dump_table(self):
        print "========================================="
        print '---------------SYMBOL TABLE--------------'
        print "========================================="
        print 'Name, Type, Value'
        for name, element in self.hash_table.iteritems():
            element.display()
        print "========================================="
