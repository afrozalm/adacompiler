from copy import deepcopy

basic_types = ['INTEGER', 'FLOAT', 'CHAR',
               'BOOLEAN', 'NULL', 'ERROR', 'STATIC']
complex_types = ['ARRAY', 'FUNCTION', 'RECORD', 'PROCEDURE']


class Element(object):
    def __init__(self, Name=None, Type=None, Value=None,
                 Mode=None):
        self.Name = Name
        self.Type = Type
        self.Value = Value
        self.Mode = Mode  # ['IN', 'OUT', 'IO', 'ACCESS']

    def operate(self, operator, operants, p):
        '''
        operants : a list of elements on which the operator acts.

        returns true is successful
                false is error and prints error
        '''
        err_msg_prefix = 'Error detected at line : ' + str(p.lineno) + ' '
        if operator in ['*', '+', '-']:
            if len(operants) == 2:
                op1, op2 = operants
                valid = ['INTEGER', 'FLOAT']
                if op1.Type.type_name in valid and op2.Type.type_name in valid:
                    if op1.Type.type_name == 'INTEGER' and op1.Type.type_name == 'INTEGER':
                        self.Type = Type(type_name = 'INTEGER')
                    else:
                        self.Type = Type(type_name = 'FLOAT')
            else:
                print err_msg_prefix \
                    + operator \
                    + ' operator accepts only two arguments of float/int type'
                self.Type = Type(type_name = 'ERROR')

        elif operator == '/':
            if len(operants) == 2:
                valid = ['INTEGER', 'FLOAT']
                op1, op2 = operants
                if op1.Type.type_name in valid and op2.Type.type_name in valid:
                    if op2.Value == 0:
                        self.Type = Type(type_name = 'ERROR')
                        print 'Divide by zero error detected'
                    else:
                        if op1.Type.type_name == 'INTEGER' and op1.Type.type_name == 'INTEGER':
                            self.Type = Type(type_name = 'INTEGER')
                        else:
                            self.Type = Type(type_name = 'FLOAT')
            else:
                print err_msg_prefix \
                    + '/ operator accepts only two arguments of float/int type'
                self.Type = Type(type_name = 'ERROR')

        elif operator == '**':
            if len(operants) == 2:
                valid = ['INTEGER', 'FLOAT']
                op1, op2 = operants
                if op1.Type.type_name in valid and op2.Type.type_name in valid:
                    self.Value = op1.Value ** op2.Value
                    if isinstance(self.Value, int):
                        self.Type = Type(type_name = 'INTEGER')
                    else:
                        self.Type = Type(type_name = 'FLOAT')
            else:
                print err_msg_prefix \
                    + operator \
                    + ' operator accepts only two arguments of float/int type'
                self.Type = Type(type_name = 'ERROR')

        elif operator.upper() in ['REM', 'MOD']:
            op1, op2 = operants
            if op1.Type.type_name == 'INTEGER' \
               and op2.Type.type_name == 'INTEGER':
                self.Value = op1.Value % op2.Value
            else:
                print err_msg_prefix \
                    + operator + ' operator accepts arguments of type integer'
                self.Type.type_name = 'ERROR'

        elif operator.upper() in ['AND', 'OR', 'XOR']:
            op1, op2 = operants
            if op1.Type.type_name == 'BOOLEAN' \
               and op2.Type.type_name == 'BOOLEAN':
                self.Type.type_name = 'BOOLEAN'
                if operator.upper() == 'AND':
                    self.value = op1.Value and op2.Value
                elif operator.upper() == 'OR':
                    self.value = op1.Value or op2.Value
                else:
                    self.value = op1.Value ^ op2.Value
            else:
                print err_msg_prefix \
                    + operator + ' operator accepts arguments of type integer'
                self.Type.type_name = 'ERROR'

        elif operator == '+':
            if len(operants) == 2:
                op1, op2 = operants
                valid = ['INTEGER', 'FLOAT']
                if op1.Type.type_name in valid and op2.Type.type_name in valid:
                    self.Value = op1.Value + op2.Value
                if isinstance(self.Value, int):
                    self.Type = Type(type_name='INTEGER')
                else:
                    self.Type = Type(type_name='FLOAT')
            elif len(operants) == 1:
                if operants[0].Type.type_name in ['INTEGER', 'FLOAT']:
                    self = deepcopy(operator[0])
                else:
                    self.Type.type_name = 'ERROR'
                    print err_msg_prefix \
                        + '+ operator accepts argument of type float/integer'
            else:
                print err_msg_prefix \
                    + '+ operator accepts  arguments of float/int type'
                self.Type = Type(type_name='ERROR')

        elif operator in ['<', '<=', '>', '>=', '=', '/=']:
            if len(operants) == 2:
                op1, op2 = operants
                int_fl = ['INTEGER', 'FLOAT']
                cond1 = op1.Type.type_name in int_fl
                cond2 = op2.Type.type_name in int_fl
                cond3 = op1.Type.type_name == 'ARRAY' \
                    and op1.constituent_type == 'CHAR'
                cond4 = op2.Type.type_name == 'ARRAY' \
                    and op2.constituent_type == 'CHAR'
                cond5 = cond1 and cond2
                cond6 = cond3 and cond4

                if cond5 or cond6:
                    self.Type = Type(type_name='BOOLEAN')

                if op1.Value is not None and op2.Value is not None:
                    if operator == '=':
                        self.Value = (op1.Value == op2.Value)
                    elif operator == '/=':
                        self.Value = not (op1.Value == op2.Value)
                    elif operator == '>':
                        self.Value = (op1.Value > op2.Value)
                    elif operator == '>=':
                        self.Value = (op1.Value >= op2.Value)
                    elif operator == '<':
                        self.Value = (op1.Value < op2.Value)
                    else:
                        self.Value = (op1.Value <= op2.Value)
            else:
                print err_msg_prefix \
                    + operator \
                    + ' operates on two arguments of int/float/string'

        elif operator == '-':
            if len(operants) == 2:
                op1, op2 = operants
                valid = ['INTEGER', 'FLOAT']
                if op1.Type.type_name in valid and op2.Type.type_name in valid:
                    self.Value = op1.Value - op2.Value
                if isinstance(self.Value, int):
                    self.Type = Type(type_name='INTEGER')
                else:
                    self.Type = Type(type_name='FLOAT')
            elif len(operants) == 1:
                if operants[0].Type.type_name in ['INTEGER', 'FLOAT']:
                    self.Type = deepcopy(operants[0].Type)
                    self.Value = -1 * operants[0].Value
                else:
                    self.Type.type_name = 'ERROR'
                    print err_msg_prefix \
                        + '- operator accepts argument of type float/integer'
            else:
                print err_msg_prefix \
                    + '- operator accepts  arguments of float/int type'
                self.Type.type_name = 'ERROR'

        elif operator == '&':
            if len(operator) == 2:
                op1, op2 = operants
                if op1.Type.type_name == 'ARRAY' \
                   and op2.Type.type_name == 'ARRAY':

                    self.Type.type_name = 'ARRAY'
                    if op1.Type.constituent_type == op2.Type.constituent_type:
                        self.Value = op1.Value + op2.Value
                        self.Type.constituent_type = op1.Type.constituent_type
                    else:
                        print err_msg_prefix \
                            + '[Type mismatch] both arrays should be of same type'
                else:
                    print err_msg_prefix \
                        + '& operator accepts two arguments of array type'
                    self.Type.type_name = 'ERROR'
            else:
                print err_msg_prefix \
                    + '& operator accepts two arguments of array type'
                self.Type.type_name = 'ERROR'

        elif operator.upper() == 'NOT':
            if len(operants) == 1:
                op = operants[0]
                if op.Type.type_name == 'BOOLEAN':
                    self.Type.type_name = 'BOOLEAN'
                    self.Value = not op.Value
                else:
                    print err_msg_prefix \
                        + 'NOT operator operates on boolean values only'
            else:
                print err_msg_prefix \
                    + 'NOT operator takes a single operant'

        elif operator.upper() == 'ABS':
            if len(operants) == 1:
                op = operants[0]
                if op.Type.type_name in ['INTEGER', 'FLOAT']:
                    self.Type.type_name = op.Type.type_name
                    self.Value = abs(op.Value)
                else:
                    print err_msg_prefix \
                        + 'ABS operator operates on integer and float values'
            else:
                print err_msg_prefix \
                    + 'ABS operator takes a single operant'

        else:
            print 'operator ' + operator + ' not handled'
            return False

    def assign_init_value(self, value):
        if self.Type.type_name == 'ARRAY':
            if isinstance(value, int) or isinstance(value, float):
                cond_v = True
            elif isinstance(value, str) and len(value) == 1:
                cond_c = True
            else:
                cond_v = False
                cond_c = False

            if self.Type.constituent_type in ['INTEGER', 'FLOAT'] and cond_v:
                if value > self.Type.low and value < self.Type.high:
                    for ele in self.Value:
                        ele.Value = value
                    return True
                else:
                    print "Value is out of constraint range : [" \
                        + str(self.low) + ', ' + str(self.high) + ']'
                    return False
            elif self.Type.constituent_type == 'CHAR' and cond_c:
                for ele in self.Value:
                    ele.Value = value
            else:
                print "Cannot initialize no strings or int/float arrays"
                return False
        elif self.Type.type_name == 'INTEGER':
            if isinstance(value, int):
                self.Value = value
                return True
            else:
                print str(value) + ' is not an integer'
                return False
        elif self.Type.type_name == 'FLOAT':
            if isinstance(value, float):
                self.Value = value
                return True
            else:
                print str(value) + ' is not a float instance'
                return False
        elif self.Type.type_name == 'CHAR':
            if isinstance(value, str) and len(value) == 1:
                self.Value = value
                return True
            else:
                print str(value) + ' is not char type'
                return False
        elif self.Type.type_name == 'BOOLEAN':
            if isinstance(value, bool):
                self.Value = value
                return True
            else:
                print str(value) + ' is not boolean type'
                return False
        else:
            return False

    def get_attrs(self):
        output = ''
        if self.Name is not None:
            output += self.Name
        else:
            output += '<not assigned>'

        if self.Type is not None:
            if self.Type.type_name in basic_types:
                output += ', ' + self.Type.type_name
            elif self.Type.type_name == 'ARRAY':
                output += ', array of ' + self.Type.constituent_type \
                          + ' with dimensionality ' + str(self.Type.dim)
            elif self.Type.type_name is not None:
                output += ', ' + self.Type.type_name
            else:
                output += ', <not assigned>'
        else:
            output += ', <not assigned>'

        if self.Value is not None:
            if self.Type.type_name in basic_types:
                output += ', ' + str(self.Value)
            elif self.Type.type_name == 'ARRAY' and self.Type.constituent_type == 'CHAR':
                output += ', ' + self.Value
            elif self.Type.type_name == 'ARRAY' and not self.Type.constituent_type == 'CHAR':
                output += ', <non char array cannot be displayed>'
            else:
                output += ', <cannot display complex types except char array>'
        else:
            output += ', <not assigned>'

        return output


class Type(object):
    def __init__(self, type_name=None,
                 low=None, high=None, size=None, constituent_type=None,
                 dim=None, dim_ranges=[], is_range_set=False,
                 is_const=False, is_aliased=False,
                 params=None):
        self.type_name = type_name
        '''is_const is used for identifiers which as statically defined'''
        self.is_const = is_const
        self.is_aliased = is_aliased
        '''dim_ranges is a list of tuples used for stroing allowed ranges in
        different dimesions'''
        self.dim_ranges = dim_ranges
        '''the dimensionality of array'''
        self.dim = dim
        '''Total size of the array = d0 x d1 x d2 x....'''
        self.size = size
        '''high, low and is_range_set are used for ranges'''
        self.high = high
        self.low = low
        self.is_range_set = is_range_set
        '''constituent_type is used for array type'''
        self.constituent_type = constituent_type
        '''for procedure types holding list of elements
        with name value type moade set'''
        self.params = params
