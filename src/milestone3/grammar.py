from elements import Element, Type, basic_types
from copy import deepcopy
from symbol_table import SymbolTable


class Rules(object):
    def p_start_symbol(self, p):
        '''start_symbol : compilation
        '''
        if self.debug: print "rule 1"
        if self.successful:
            self.symb_output += self.symbol_table.add_symb_output()
            print "========================================="
            print '               SYMBOL TABLE              '
            print "========================================="
            print 'Name, Type, Offset, Value'
            print self.symb_output
            print "========================================="
            self.three_add_code.display_code()

    def p_pragma(self, p):
        '''pragma  : PRAGMA IDENTIFIER ';'
           | PRAGMA simple_name '(' pragma_arg_s ')' ';'
        '''
        if self.debug: print "rule 2"

    def p_pragma_arg_s(self, p):
        '''pragma_arg_s : pragma_arg
           | pragma_arg_s ',' pragma_arg
        '''
        if self.debug: print "rule 3"

    def p_pragma_arg(self, p):
        '''pragma_arg : expression
           | simple_name ARROW expression
        '''
        if self.debug: print "rule 4"

    def p_pragma_s(self, p):
        '''pragma_s :
           | pragma_s pragma
        '''
        if self.debug: print "rule 5"

    def p_decl(self, p):
        '''decl    : object_decl
           | number_decl
           | type_decl
           | subtype_decl
           | subprog_decl
           | pkg_decl
           | task_decl
           | prot_decl
           | exception_decl
           | rename_decl
           | generic_decl
           | body_stub
           | error ';'
        '''
        if self.debug: print "rule 6"
        p[0] = p[1]

    def p_object_decl(self, p):
        '''object_decl : def_id_s ':' object_qualifier_opt object_subtype_def init_opt ';'
        '''
        #     0             1      2       3                    4             5
        if self.debug: print "rule 7"
        if p[5] is not None:  # when init_opt is not empty
            if p[5].Type.type_name == 'ARRAY':
                self.p_error(p)
                print 'Array assignment not handled yet'
            else:
                value = p[5]
                elements = []
                for ele in p[1]:
                    ele.Type = deepcopy(p[4].Type)
                    ele.Type.is_const, ele.Type.is_aliased = p[3]
                    if ele.Type.type_name == value.Type.type_name:
                        ele.Value = p[5].Value
                        ele.Type.assign_width()
                        ele.Offset = self.symbol_table.get_width()
                        self.symbol_table.add_to_table(ele.Name, ele, p.lineno)
                        self.symbol_table.add_width(ele.Type.width)
                        self.three_add_code.emit([ele.Name, p[5].Value, ':=', None])
                        elements += [ele]
                    else:
                        self.p_error(p)
                        print 'Declaration type and init type do not match'
        else:
            elements = []
            for ele in p[1]:
                ele.Type = deepcopy(p[4].Type)
                ele.Type.is_const, ele.Type.is_aliased = p[3]
                # ele.Value = p[5].Value
                ele.Type.assign_width()
                ele.Offset = self.symbol_table.get_width()
                if self.symbol_table.add_to_table(ele.Name, ele, p.lineno):
                    self.symbol_table.add_width(ele.Type.width)
                    elements += [ele]
                else:
                    self.p_error(p)
        p[0] = elements

    def p_def_id_s(self, p):
        '''def_id_s : def_id
           | def_id_s ',' def_id
        '''
        if self.debug: print "rule 8"
        if len(p) == 2:
            p[0] = p[1]
        else:
            p[0] = p[1] + p[3]

    def p_def_id(self, p):
        '''def_id  : IDENTIFIER
        '''
        p[0] = [Element(Name=p[1])]

    def p_object_qualifier_opt(self, p):
        '''object_qualifier_opt :
           | ALIASED
           | CONSTANT
           | ALIASED CONSTANT
        '''
        if self.debug: print "rule 9"
        if len(p) == 2:
            if p[1].upper() == 'ALIASED':
                p[0] = (True, False)
            else:
                p[0] = (False, True)
        elif len(p) == 1:
            p[0] = (False, False)
        else:
            p[0] = (True, True)

    def p_object_subtype_def(self, p):
        '''object_subtype_def : subtype_ind
           | array_type
        '''
        if self.debug: print "rule 10"
        p[0] = p[1]

    def p_init_opt(self, p):
        '''init_opt :
           | ASSIGNMENT expression
        '''
        if self.debug: print "rule 11"
        if len(p) == 3:
            p[0] = p[2]
        else:
            p[0] = None

    def p_number_decl(self, p):
        '''number_decl : def_id_s ':' CONSTANT ASSIGNMENT expression ';'
        '''
        #      0            1      2      3        4          5
        if self.debug: print "rule 12"
        p[0] = []
        if p[5].Type.type_name in basic_types:
            for ele in p[1]:
                ele.Type.type_name = p[5].Type.type_name
                ele.Type.is_const = True
                ele.Value = p[5].Value
                ele.Type.assign_width()
                ele.Offset = self.symbol_table.get_width()
                if self.symbol_table.add_to_table(ele.Name, ele, p.lineno):
                    self.symbol_table.add_width(ele.width)
                    self.three_add_code.emit([ele.Name, ele.Value, ':=', None])
                    p[0] += [ele]
                else:
                    self.p_error(p)
        else:
            self.p_error(p)
            print 'Only basic types can be assigned'

    def p_type_decl(self, p):
        '''type_decl : TYPE IDENTIFIER discrim_part_opt type_completion ';'
        '''
        if self.debug: print "rule 13"

    def p_discrim_part_opt(self, p):
        '''discrim_part_opt :
           | discrim_part
           | '(' BOX ')'
        '''
        if self.debug: print "rule 14"

    def p_type_completion(self, p):
        '''type_completion :
           | IS type_def
        '''
        if self.debug: print "rule 14"

    def p_type_def(self, p):
        '''type_def : enumeration_type
           | integer_type
           | real_type
           | array_type
           | record_type
           | access_type
           | derived_type
           | private_type
        '''
        if self.debug: print "rule 15"

    def p_subtype_decl(self, p):
        '''subtype_decl : SUBTYPE IDENTIFIER IS subtype_ind ';'
        '''
        if self.debug: print "rule 16"

    def p_subtype_ind(self, p):
        '''subtype_ind : name constraint
           | name
        '''
        if self.debug: print "rule 17"
        if len(p) == 2:
            if p[1].Name.upper() in basic_types:
                curr_type = Type(type_name=p[1].Name.upper())
                curr_type.assign_width()
                p[0] = Element(Type=curr_type)
            else:
                self.p_error(p)
                print 'Only basic types implemented'
        else:
            if p[1].Name.upper() in basic_types:
                curr_type = Type(type_name=p[1].upper(),
                                 low=p[2][0],
                                 high=p[2][1],
                                 is_range_set=True)
                curr_type.assign_width()
                p[0] = Element(Type=curr_type)
            else:
                self.p_error(p)
                print 'Only basic constraints supported'

    def p_constraint(self, p):
        '''constraint : range_constraint
           | decimal_digits_constraint
        '''
        if self.debug: print "rule 18"
        p[0] = p[1]

    def p_decimal_digits_constraint(self, p):
        '''decimal_digits_constraint : DIGITS expression range_constr_opt
        '''
        if self.debug: print "rule 20"

    def p_derived_type(self, p):
        '''derived_type : NEW subtype_ind
           | NEW subtype_ind WITH PRIVATE
           | NEW subtype_ind WITH record_def
           | ABSTRACT NEW subtype_ind WITH PRIVATE
           | ABSTRACT NEW subtype_ind WITH record_def
        '''
        if self.debug: print "rule 21"

    def p_range_constraint(self, p):
        '''range_constraint : RANGE range
        '''
        if self.debug: print "rule 22"
        p[0] = p[2]

    def p_range(self, p):
        '''range : simple_expression DOUBLEDOT simple_expression
               | name TICK RANGE
               | name TICK RANGE '(' expression ')'
        '''
        if self.debug: print "rule 23"
        if len(p) == 4:  # first case
            p1_val = p[1].Name if p[1].Name is not None else p[1].Value
            p3_val = p[3].Name if p[3].Name is not None else p[3].Value
            p[0] = (p1_val, p3_val)
        else:
            self.p_error(p)
            print 'This range calling is not handled yet'

    def p_enumeration_type(self, p):
        '''enumeration_type : '(' enum_id_s ')'
        '''
        if self.debug: print "rule 24"

    def p_enum_id_s(self, p):
        '''enum_id_s : enum_id
           | enum_id_s ',' enum_id
        '''
        if self.debug: print "rule 25"

    def p_enum_id(self, p):
        '''enum_id : IDENTIFIER
           | CHAR
        '''
        if self.debug: print "rule 26"

    def p_integer_type(self, p):
        '''integer_type : range_spec
           | MOD expression
        '''
        if self.debug: print "rule 27"
        if len(p) == 2:
            p[0] = p[1]

    def p_range_spec(self, p):
        '''range_spec : range_constraint
        '''
        if self.debug: print "rule 28"
        p[0] = p[1]

    def p_range_spec_opt(self, p):
        '''range_spec_opt :
           | range_spec
        '''
        if self.debug: print "rule 29"
        if len(p) == 2:
            p[0] = p[1]

    def p_real_type(self, p):
        '''real_type : float_type
           | fixed_type
        '''
        if self.debug: print "rule 30"

    def p_float_type(self, p):
        '''float_type : DIGITS expression range_spec_opt
        '''
        if self.debug: print "rule 31"

    def p_fixed_type(self, p):
        '''fixed_type : DELTA expression range_spec
           | DELTA expression DIGITS expression range_spec_opt
        '''
        if self.debug: print "rule 32"

    def p_array_type(self, p):
        '''array_type : unconstr_array_type
           | constr_array_type
        '''
        if self.debug: print "rule 33"
        p[0] = p[1]

    def p_unconstr_array_type(self, p):
        '''unconstr_array_type : ARRAY '(' index_s ')' OF component_subtype_def
        '''
        if self.debug: print "rule 34"

    def p_constr_array_type(self, p):
        '''constr_array_type : ARRAY iter_index_constraint OF component_subtype_def
        '''
        #           0            1              2          3             4
        if self.debug: print "rule 35"
        curr_type = Type(type_name='ARRAY',
                         constituent_type=p[4].Type.type_name,
                         dim=len(p[2]),
                         is_range_set=p[4].Type.is_range_set,
                         high=p[4].Type.high,
                         low=p[4].Type.low,
                         dim_ranges=p[2])
        curr_type.assign_width()
        p[0] = Element(Type=curr_type)

    def p_component_subtype_def(self, p):
        '''component_subtype_def : aliased_opt subtype_ind
        '''
        if self.debug: print "rule 36"
        p[0] = p[2]

    def p_aliased_opt(self, p):
        '''aliased_opt :
           | ALIASED
        '''
        if self.debug: print "rule 37"
        if len(p) == 1:
            p[0] = False
        else:
            p[0] = True

    def p_index_s(self, p):
        '''index_s : index
           | index_s ',' index
        '''
        if self.debug: print "rule 38"

    def p_index(self, p):
        '''index : name RANGE BOX
        '''
        if self.debug: print "rule 39"

    def p_iter_index_constraint(self, p):
        '''iter_index_constraint : '(' iter_discrete_range_s ')'
        '''
        if self.debug: print "rule 40"
        p[0] = p[2]

    def p_iter_discrete_range_s(self, p):
        '''iter_discrete_range_s : discrete_range
           | iter_discrete_range_s ',' discrete_range
        '''
        if self.debug: print "rule 41"
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1] + [p[3]]

    def p_discrete_range(self, p):
        '''discrete_range : name range_constr_opt
           | range
        '''
        if self.debug: print "rule 42"
        if len(p) == 2:
            if isinstance(p[1][0], int) and isinstance(p[1][1], int):
                p[0] = p[1]
            else:
                cond1 = False
                cond2 =False
                low_name = self.symbol_table.lookup(p[1][0])
                if low_name is not None:
                    cond1 = (low_name.Type.type_name == 'INTEGER')
                high_name = self.symbol_table.lookup(p[1][1])
                if high_name is not None:
                    cond2 = (high_name.Type.type_name == 'INTEGER')
                if cond1 and cond2:
                    p[0] = p[1]
                else:
                    self.p_error(p)
                    print 'Only discrete values allowed'
        else:
            if p[1].Name.upper() == 'INTEGER':
                p[0] = p[2]
            else:
                print 'only integer values allowed in discrete range'

    def p_range_constr_opt(self, p):
        '''range_constr_opt :
           | range_constraint
        '''
        if self.debug: print "rule 43"
        if len(p) == 2:
            p[0] = p[1]

    def p_record_type(self, p):
        '''record_type : tagged_opt limited_opt record_def
        '''
        if self.debug: print "rule 44"

    def p_record_def(self, p):
        '''record_def : RECORD pragma_s comp_list END RECORD
           | NULL RECORD
        '''
        if self.debug: print "rule 45"

    def p_tagged_opt(self, p):
        '''tagged_opt :
           | TAGGED
           | ABSTRACT TAGGED
        '''
        if self.debug: print "rule 46"

    def p_comp_list(self, p):
        '''comp_list : comp_decl_s variant_part_opt
           | variant_part pragma_s
           | NULL ';' pragma_s
        '''
        if self.debug: print "rule 47"

    def p_comp_decl_s(self, p):
        '''comp_decl_s : comp_decl
           | comp_decl_s pragma_s comp_decl
        '''
        if self.debug: print "rule 48"

    def p_variant_part_opt(self, p):
        '''variant_part_opt : pragma_s
           | pragma_s variant_part pragma_s
        '''
        if self.debug: print "rule 49"

    def p_comp_decl(self, p):
        '''comp_decl : def_id_s ':' component_subtype_def init_opt ';'
           | error ';'
        '''
        if self.debug: print "rule 50"

    def p_discrim_part(self, p):
        '''discrim_part : '(' discrim_spec_s ')'
        '''
        if self.debug: print "rule 51"

    def p_discrim_spec_s(self, p):
        '''discrim_spec_s : discrim_spec
           | discrim_spec_s ';' discrim_spec
        '''
        if self.debug: print "rule 51"

    def p_discrim_spec(self, p):
        '''discrim_spec : def_id_s ':' access_opt mark init_opt
           | error
        '''
        if self.debug: print "rule 52"

    def p_access_opt(self, p):
        '''access_opt :
           | ACCESS
        '''
        if self.debug: print "rule 53"

    def p_variant_part(self, p):
        '''variant_part : CASE simple_name IS pragma_s variant_s END CASE ';'
        '''
        if self.debug: print "rule 54"

    def p_variant_s(self, p):
        '''variant_s : variant
           | variant_s variant
        '''
        if self.debug: print "rule 55"

    def p_variant(self, p):
        '''variant : WHEN choice_s ARROW pragma_s comp_list
        '''
        if self.debug: print "rule 56"

    def p_choice_s(self, p):
        '''choice_s : choice
           | choice_s '|' choice
        '''
        if self.debug: print "rule 57"

    def p_choice(self, p):
        '''choice : expression
           | discrete_with_range
           | OTHERS
        '''
        if self.debug: print "rule 58"

    def p_discrete_with_range(self, p):
        '''discrete_with_range : name range_constraint
           | range
        '''
        if self.debug: print "rule 59"

    def p_access_type(self, p):
        '''access_type : ACCESS subtype_ind
           | ACCESS CONSTANT subtype_ind
           | ACCESS ALL subtype_ind
           | ACCESS prot_opt PROCEDURE formal_part_opt
           | ACCESS prot_opt FUNCTION formal_part_opt RETURN mark
        '''
        if self.debug: print "rule 60"

    def p_prot_opt(self, p):
        '''prot_opt :
           | PROTECTED
        '''
        if self.debug: print "rule 61"

    def p_decl_part(self, p):
        '''decl_part :
           | decl_item_or_body_s1
        '''
        if self.debug: print "rule 62"
        if len(p) == 2:
            p[0] = p[1]

    def p_decl_item_s(self, p):
        '''decl_item_s :
           | decl_item_s1
        '''
        if self.debug: print "rule 63"
        if len(p) == 2:
            p[0] = p[1]

    def p_decl_item_s1(self, p):
        '''decl_item_s1 : decl_item
           | decl_item_s1 decl_item
        '''
        if self.debug: print "rule 64"
        if len(p) == 2:
            p[0] = p[1]
        else:
            p[0] = p[1] + p[2]

    def p_decl_item(self, p):
        '''decl_item : decl
           | use_clause
           | rep_spec
           | pragma
        '''
        if self.debug: print "rule 65"
        p[0] = p[1]

    def p_decl_item_or_body_s1(self, p):
        '''decl_item_or_body_s1 : decl_item_or_body
           | decl_item_or_body_s1 decl_item_or_body
        '''
        if self.debug: print "rule 66"
        if len(p) == 2:
            p[0] = p[1]
        else:
            if p[1] is None or p[2] is None:
                if p[1] is None:
                    p[0] = p[2]
                else:
                    p[0] = p[1]
            else:
                p[0] = p[1] + p[2]

    def p_decl_item_or_body(self, p):
        '''decl_item_or_body : body
           | decl_item
        '''
        if self.debug: print "rule 67a"
        if not isinstance(p[1], list):
            p[0] = [p[1]]
        else:
            p[0] = p[1]

    def p_body(self, p):
        '''body : subprog_body
           | pkg_body
           | task_body
           | prot_body
        '''
        if self.debug: print "rule 67b"
        # not doing pkg_body, task_body, prot_body
        if p[1] is not None:
            p[0] = p[1]
        else:
            print "Not handled yet"

    def p_name(self, p):
        '''name : simple_name
           | indexed_comp
           | selected_comp
           | attribute
           | operator_symbol
        '''
        if self.debug: print "rule 68"
        p[0] = p[1]

    def p_mark(self, p):
        '''mark : simple_name
           | mark TICK attribute_id
           | mark '.' simple_name
        '''
        if self.debug: print "rule 69"
        if len(p) == 2:
            p[0] = p[1]
        else:
            print 'not implemented yet'

    def p_simple_name(self, p):
        '''simple_name : IDENTIFIER
        '''
        if self.debug: print "rule 70"
        if self.symbol_table.is_present_in_scope(p[1].lower()):
            p[0] = self.symbol_table.lookup(p[1].lower())
        else:
            if p[1].lower() == 'true':
                curr_type = Type(type_name='BOOLEAN', width=4)
                p[0] = Element(Type=curr_type, Value=True)
            elif p[1].lower() == 'false':
                curr_type = Type(type_name='BOOLEAN', width=4)
                p[0] = Element(Type=curr_type, Value=False)
            else:
                p[0] = Element(Name=p[1])

    def p_compound_name(self, p):
        '''compound_name : simple_name
           | compound_name '.' simple_name
        '''
        if self.debug: print "rule 71"
        if len(p) == 2:
            p[0] = p[1]
        else:
            print 'Not handled yet'

    def p_c_name_list(self, p):
        '''c_name_list : compound_name
        | c_name_list ',' compound_name
        '''
        if self.debug: print "rule 72"
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1] + [p[3]]

    def p_used_char(self, p):
        '''used_char : CHAR
        '''
        if self.debug: print "rule 73"
        p[0] = p[1]

    def p_operator_symbol(self, p):
        '''operator_symbol : STRING
        '''
        if self.debug: print "rule 74"

    def p_indexed_comp(self, p):
        '''indexed_comp : name '(' value_s ')'
        '''
        #       0           1   2     3     4
        if self.debug: print "rule 75"
        ele = self.symbol_table.lookup(p[1].Name)
        if ele is not None:
            if ele.Type.type_name == 'ARRAY':
                cond_not_integral = False
                for v in p[3]:
                    if not (isinstance(v.Value, int) or v.Type.type_name == 'INTEGER'):
                        cond_not_integral = True
                if cond_not_integral:
                        self.p_error(p)
                        print 'Only integral indexes allowed'
                elif len(p[3]) == ele.Type.dim:
                    limits = ele.Type.dim_ranges
                    temp1 = \
                        self.three_add_code.new_temp(Type(type_name='INTEGER'), self.symbol_table)
                    self.symbol_table.add_to_table(temp1.Name, temp1, p.lineno)
                    temp2 = \
                        self.three_add_code.new_temp(Type(type_name='INTEGER'), self.symbol_table)
                    self.symbol_table.add_to_table(temp2.Name, temp2, p.lineno)
                    self.three_add_code.emit([temp1.Name,
                                              p[3][0].Value,
                                              'int_-',
                                              limits[0][0]])
                    for i in xrange(1, len(p[3]) - 1):
                        self.three_add_code.emit([temp2.Name,
                                                  temp1.Name,
                                                  'int_*',
                                                  limits[i][1] - limits[i][0] + 1])
                        self.three_add_code.emit([temp1.Name,
                                                  temp2.Name,
                                                  'int_+',
                                                  p[3][i].Value - limits[i][0]])

                    self.three_add_code.emit([temp2.Name,
                                              temp1.Name,
                                              'int_+',
                                              p[3][-1].Value - limits[-1][0]])

                    curr_type = Type(type_name=ele.Type.constituent_type)
                    curr_type.assign_width()
                    self.three_add_code.emit([temp1.Name,
                                              temp2.Name,
                                              'int_*',
                                              curr_type.width])
                    p[0] = Element(Name=ele.Name + '[' + temp1.Name + ']',
                                   Type=curr_type)
                else:
                    self.p_error(p)
                    print 'Dimension mismatch'
            elif ele.Type.type_name == 'PROCEDURE':
                # will do when return type of procudure is fixed
                if len(p[3]) == len(ele.Type.params):
                    correct_types = True
                    for p1, p2 in zip(p[3], ele.Type.params):
                        if p1.Type.type_name != p2.Type.type_name:
                            correct_types = False
                    if not correct_types:
                        self.p_error(p)
                        print 'Argument types do not match'
                    else:
                        param_list = ele.Type.params
                        for p in param_list:
                            self.three_add_code.emit(['param',
                                                      p.Name,
                                                      p.Mode,
                                                      None])
                        self.three_add_code.emit(['call_proced',
                                                  ele.Name,
                                                  None,
                                                  None])
                else:
                    self.p_error(p)
                    print str(len(ele.Type.params)) + ' expected, got ' \
                        + str(len(p[3]))
            elif ele.Type.type_name == 'FUNCTION':
                if len(p[3]) == len(ele.Type.params):
                    flag = False
                    for val, param in zip(p[3], ele.Type.params):
                        if not val.Type.type_name == param.Type.type_name:
                            flag = True
                            print val.Name + ' should be ' \
                                    + param.Type.type_name + ' type'
                    if flag:
                        self.p_error(p)
                else:
                    self.p_error(p)
                    print str(len(ele.params)) + ' parameters expected, ' \
                            + str(len(p[3])) + ' given'
            else:
                self.p_error(p)
                print 'Cannot index' + ele.Name
        else:
            self.p_error(p)
            print p[1].Name + ' not in scope'

    def p_value_s(self, p):
        '''value_s : value
           | value_s ',' value
        '''
        if self.debug: print "rule 76"
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1] + [p[3]]

    def p_value(self, p):
        '''value : expression
           | comp_assoc
           | discrete_with_range
           | error
        '''
        if self.debug: print "rule 77"
        p[0] = p[1]

    def p_selected_comp(self, p):
        '''selected_comp : name '.' simple_name
           | name '.' used_char
           | name '.' operator_symbol
           | name '.' ALL
        '''
        if self.debug: print "rule 78"

    def p_attribute(self, p):
        '''attribute : name TICK attribute_id
        '''
        if self.debug: print "rule 79"

    def p_attribute_id(self, p):
        '''attribute_id : IDENTIFIER
           | DIGITS
           | DELTA
           | ACCESS
        '''
        if self.debug: print "rule 80"

    def p_literal(self, p):
        '''literal : literal_int
           | literal_base_int
           | literal_float
           | literal_base_float
           | literal_used_char
           | literal_null
        '''
        if self.debug: print "rule 81"
        p[0] = p[1]

    def p_literal_int(self, p):
        '''literal_int : INTEGER
        '''
        if self.debug: print "rule 82"
        int_type = Type(type_name='INTEGER', width=4)
        p[0] = Element(Value=p[1], Type=int_type)

    def p_literal_base_int(self, p):
        '''literal_base_int : BASE_INTEGER
        '''
        if self.debug: print "rule 83"
        int_type = Type(type_name='INTEGER', width=4)
        p[0] = Element(Value=p[1], Type=int_type)

    def p_literal_float(self, p):
        '''literal_float : FLOAT
        '''
        if self.debug: print "rule 84"
        float_type = Type(type_name='FLOAT', width=8)
        p[0] = Element(Value=p[1], Type=float_type)

    def p_literal_base_float(self, p):
        '''literal_base_float : BASE_FLOAT
        '''
        if self.debug: print "rule 85"
        float_type = Type(type_name='FLOAT', width=8)
        p[0] = Element(Value=p[1], Type=float_type)

    def p_literal_used_char(self, p):
        '''literal_used_char : used_char

        '''
        if self.debug: print "rule 86"
        char_type = Type(type_name='CHAR', width=1)
        p[0] = Element(Value=p[1], Type=char_type)

    def p_literal_null(self, p):
        '''literal_null : NULL
        '''
        if self.debug: print "rule 87"
        null_type = Type(type_name='NULL', width=0)
        p[0] = Element(Type=null_type)

    def p_aggregate(self, p):
        '''aggregate : '(' comp_assoc ')'
           | '(' value_s_2 ')'
           | '(' expression WITH value_s ')'
           | '(' expression WITH NULL RECORD ')'
           | '(' NULL RECORD ')'
        '''
        if self.debug: print "rule 88"

    def p_value_s_2(self, p):
        '''value_s_2 : value ',' value
           | value_s_2 ',' value
        '''
        if self.debug: print "rule 89"

    def p_comp_assoc(self, p):
        '''comp_assoc : choice_s ARROW expression
        '''
        if self.debug: print "rule 90"

    def p_m(self, p):
        ''' m :
        '''
        p[0] = self.three_add_code.instr_no

    def p_expression(self, p):
        '''expression : relation
           | expression logical m relation
           | expression short_circuit relation
        '''
        if self.debug: print "rule 91"
        if len(p) == 2:
            p[0] = p[1]
        else:
            op1_name = p[1].Type.type_name
            op2_name = p[3].Type.type_name
            valid = ['INTEGER', 'FLOAT']
            cond = op1_name in valid and op2_name in valid
            if cond:
                temp_type = Type(type_name='BOOLEAN')
                p[0] = self.three_add_code.new_temp(temp_type, self.symbol_table)
                self.symbol_table.add_to_table(p[0].Name, p[0], p.lineno)
            if p[2].lower() == 'and':
                # handle and
                p[0].truelist = p[4].truelist
                p[0].falselist = p[1].falselist + p[4].falselist
                self.three_add_code.backpatch(p[1].truelist, p[3])
            elif p[2].lower() == 'or':
                # handle or
                p[0].falselist = p[4].falselist
                p[0].truelist = p[1].truelist + p[4].truelist
                self.three_add_code.backpatch(p[1].falselist, p[3])
            else:
                print 'Short - circuits not handled yet'

    def p_logical(self, p):
        '''logical : AND
           | OR
        '''
        if self.debug: print "rule 92"
        p[0] = p[1]

    def p_short_circuit(self, p):
        '''short_circuit : AND THEN
           | OR ELSE
        '''
        if self.debug: print "rule 93"
        p[0] = p[1] + p[2]

    def p_relation(self, p):
        '''relation : simple_expression
           | simple_expression relational simple_expression
           | simple_expression membership range
           | simple_expression membership name
        '''
        if self.debug: print "rule 94"
        if len(p) == 2:
            p[0] = p[1]
        else:
            if p[2] is not None:
                ele = Element(Type=Type())
                code = ele.operate(p[2], [p[1], p[3]], p)
                temp = self.three_add_code.new_temp(ele.Type, self.symbol_table)
                self.symbol_table.add_to_table(temp.Name, temp, p.lineno)
                del ele
                if code is not None:
                    code[0] = temp.Name
                    self.three_add_code.emit(code)

                    temp.truelist = [self.three_add_code.instr_no]
                    temp.falselist = [self.three_add_code.instr_no + 1]

                    self.three_add_code.emit(['if', temp.Name, 'goto', None])
                    self.three_add_code.emit(['goto', None, None, None])

                    p[0] = temp

            else:
                print 'Membership not implemented'

    def p_relational(self, p):
        '''relational : '='
           | NOTEQUAL
           | '<'
           | LEQ
           | '>'
           | GEQ
        '''
        if self.debug: print "rule 95"
        p[0] = p[1]

    def p_membership(self, p):
        '''membership : IN
           | NOT IN
        '''
        if self.debug: print "rule 96"

    def p_simple_expression(self, p):
        '''simple_expression : unary term
           | term
           | simple_expression adding term
        '''
        if self.debug: print "rule 97"
        if len(p) == 3:
            ele = Element(Type=Type())
            code = ele.operate(p[2], [p[1], p[3]], p)
            temp = self.three_add_code.new_temp(ele.Type, self.symbol_table)
            self.symbol_table.add_to_table(temp.Name, temp, p.lineno)
            del ele
            if code is not None:
                p[0] = temp
                code[0] = temp.Name
                self.three_add_code.emit(code)

        elif len(p) == 2:
            p[0] = p[1]
        else:
            ele = Element(Type=Type())
            code = ele.operate(p[2], [p[1], p[3]], p)
            temp = self.three_add_code.new_temp(ele.Type, self.symbol_table)
            self.symbol_table.add_to_table(temp.Name, temp, p.lineno)
            del ele
            if code is not None:
                p[0] = temp
                code[0] = temp.Name
                self.three_add_code.emit(code)

    def p_unary(self, p):
        '''unary   : '+'
           | '-'
        '''
        if self.debug: print "rule 98"
        p[0] = p[1]

    def p_adding(self, p):
        '''adding  : '+'
           | '-'
           | '&'
        '''
        if self.debug: print "rule 99"
        p[0] = p[1]

    def p_term(self, p):
        '''term    : factor
           | term multiplying factor
        '''
        if self.debug: print "rule 100"
        if len(p) == 2:
            p[0] = p[1]
        else:
            ele = Element(Type=Type())
            code = ele.operate(p[2], [p[1], p[3]], p)
            temp = self.three_add_code.new_temp(ele.Type, self.symbol_table)
            self.symbol_table.add_to_table(temp.Name, temp, p.lineno)
            del ele
            if code is not None:
                p[0] = temp
                code[0] = temp.Name
                self.three_add_code.emit(code)


    def p_multiplying(self, p):
        '''multiplying : '*'
           | '/'
           | MOD
           | REM
        '''
        if self.debug: print "rule 101"
        p[0] = p[1]

    def p_factor(self, p):
        '''factor : primary
           | NOT primary
           | primary DOUBLESTAR primary
        '''
        if self.debug: print "rule 102"
        if len(p) == 2:
            p[0] = p[1]
        elif len(p) == 3:
            ele = Element(Type=Type())
            code = ele.operate(p[2], [p[1], p[3]], p)
            temp = self.three_add_code.new_temp(ele.Type, self.symbol_table)
            self.symbol_table.add_to_table(temp.Name, temp, p.lineno)
            del ele
            if code is not None:
                code[0] = temp.Name
                self.three_add_code.emit(code)

                temp.truelist = p[2].falselist
                temp.falselist = p[2].truelist

                p[0] = temp
        else:
            ele = Element(Type=Type())
            code = ele.operate(p[2], [p[1], p[3]], p)
            temp = self.three_add_code.new_temp(ele.Type, self.symbol_table)
            self.symbol_table.add_to_table(temp.Name, temp, p.lineno)
            del ele
            if code is not None:
                p[0] = temp
                code[0] = temp.Name
                self.three_add_code.emit(code)

    def p_primary(self, p):
        '''primary : literal
           | name
           | allocator
           | qualified
           | parenthesized_primary
        '''
        if self.debug: print "rule 103"
        p[0] = p[1]

    def p_parenthesized_primary(self, p):
        '''parenthesized_primary : aggregate
           | '(' expression ')'
        '''
        if self.debug: print "rule 104"
        if len(p) == 4:
            p[0] = p[2]

    def p_qualified(self, p):
        '''qualified : name TICK parenthesized_primary
        '''
        if self.debug: print "rule 105"

    def p_allocator(self, p):
        '''allocator : NEW name
           | NEW qualified
        '''
        if self.debug: print "rule 106"

    def p_statement_s(self, p):
        '''statement_s : statement
           | statement_s m statement
        '''
        if self.debug: print "rule 107"
        if len(p) == 2:
            p[0] = p[1]
        else:
            self.three_add_code.backpatch(p[1].nextlist, p[2])
            p[0] = deepcopy(p[3])

    def p_statement(self, p):
        '''statement : unlabeled
           | label statement
        '''
        if self.debug: print "rule 108"
        if len(p) == 2:
            p[0] = p[1]
        else:
            # do something to add label thing
            p[0] = p[2]

    def p_unlabeled(self, p):
        '''unlabeled : simple_stmt
           | compound_stmt
           | pragma
        '''
        if self.debug: print "rule 109"
        p[0] = p[1]

    def p_simple_stmt(self, p):
        '''simple_stmt : null_stmt
           | assign_stmt
           | exit_stmt
           | return_stmt
           | goto_stmt
           | procedure_call
           | delay_stmt
           | abort_stmt
           | raise_stmt
           | code_stmt
           | requeue_stmt
           | error ';'
        '''
        if self.debug: print "rule 110"
        if len(p) == 2:
            p[0] = p[1]
            if p[0] is None:
                p[0] = Element(nextlist=[])
            elif p[0].nextlist is None:
                p[0].nextlist = []
        else:
            self.p_error(p)
            print "Not a valid statement"

    def p_compound_stmt(self, p):
        '''compound_stmt : if_stmt
           | case_stmt
           | loop_stmt
           | block
           | accept_stmt
           | select_stmt
        '''
        if self.debug: print "rule 111"
        p[0] = p[1]

    def p_label(self, p):
        '''label : LEFTLABEL IDENTIFIER RIGHTLABEL
        '''
        if self.debug: print "rule 112"
        p[0] = p[2]

    def p_null_stmt(self, p):
        '''null_stmt : NULL ';'
        '''
        if self.debug: print "rule 113"
        p[0] = Element(nextlist=[])

    def p_assign_stmt(self, p):
        '''assign_stmt : name ASSIGNMENT expression ';'
        '''
        if self.debug: print "rule 114"
        if p[1].Name is not None or p[1].Type is not None:
            if p[1].Type.is_const:
                self.p_error(p)
                print "Cannot assign value to constant identifier"
            else:
                if not p[1].Type.type_name == p[3].Type.type_name:
                    self.p_error(p)
                    print 'Types do not match'
                elif p[1].Type.type_name not in basic_types:
                    self.p_error(p)
                    print p[1].Name + ' is not a basic type'
                elif p[3].Type.type_name not in basic_types:
                    self.p_error(p)
                    print 'expression is not a basic type'
                else:
                    p[1].Value = p[3].Value if p[3].Value is not None else p[3].Name
                    self.three_add_code.emit([p[1].Name, p[1].Value, ':=', None])
                    p[0] = Element(Type=Type(type_name='STATEMENT'),
                                   nextlist=[])
        else:
            self.p_error(p)
            print 'Invalid name/type used in this statement at line ', p.lineno(1)

    def p_if_stmt(self, p):
        '''if_stmt : IF cond_clause_s m else_opt END IF ';'
        '''
        #     0      1        2       3    4      5  6   7
        if self.debug: print "rule 115"
        p[0] = deepcopy(p[2])
        if p[4] is None:
            p[0].nextlist = p[2].nextlist + p[2].falselist
        else:
            self.three_add_code.backpatch(p[2].falselist, p[3])
            p[0].nextlist = p[2].nextlist + p[4].nextlist

    def p_cond_clause_s(self, p):
        '''cond_clause_s : cond_clause
           | cond_clause_s m ELSIF cond_clause
        '''
        if self.debug: print "rule 116"
        if len(p) == 2:
            p[0] = p[1]
        else:
            self.three_add_code.backpatch(p[1].falselist, p[2])
            p[0] = deepcopy(p[4])
            p[0].nextlist = p[4].nextlist + p[1].nextlist

    def p_n(self, p):
        '''n :
        '''
        p[0] = self.three_add_code.instr_no
        self.three_add_code.emit(["goto", None, None, None])

    def p_cond_clause(self, p):
        '''cond_clause : cond_part m statement_s n
        '''
        if self.debug: print "rule 117"
        self.three_add_code.backpatch(p[1].truelist, p[2])
        p[0] = deepcopy(p[1])
        p[0].nextlist = p[3].nextlist + [p[4]]

    def p_cond_part(self, p):
        '''cond_part : condition THEN
        '''
        if self.debug: print "rule 118"
        p[0] = deepcopy(p[1])

    def p_condition(self, p):
        '''condition : expression
        '''
        if self.debug: print "rule 119"
        if p[1].Type.type_name == 'BOOLEAN':
            p[0] = p[1]
        else:
            self.p_error(p)
            print "Only booleans can be a condition"

    def p_else_opt(self, p):
        '''else_opt :
           | ELSE statement_s
        '''
        if self.debug: print "rule 120"
        if len(p) == 1:
            p[0] = None
        else:
            p[0] = deepcopy(p[2])

    def p_case_stmt(self, p):
        '''case_stmt : case_hdr pragma_s alternative_s END CASE ';'
        '''
        if self.debug: print "rule 121"

    def p_case_hdr(self, p):
        '''case_hdr : CASE expression IS
        '''
        if self.debug: print "rule 121"

    def p_alternative_s(self, p):
        '''alternative_s :
           | alternative_s alternative
        '''
        if self.debug: print "rule 122"

    def p_alternative(self, p):
        '''alternative : WHEN choice_s ARROW statement_s
        '''
        if self.debug: print "rule 123a"

    def p_loop_stmt(self, p):
        '''loop_stmt : label_opt iteration m  basic_loop id_opt ';'
        '''
        if self.debug: print "rule 123b"
        if p[1] != p[4].Name:
            self.p_error(p)
            print 'label name and id name do not match'
        else:
            self.three_add_code.backpatch(p[4].nextlist, p[2].quad)
            self.three_add_code.backpatch(p[2].truelist, p[3])
            p[0] = Element()
            p[0].nextlist = p[2].falselist

    def p_label_opt(self, p):
        '''label_opt :
           | IDENTIFIER ':'
        '''
        if self.debug: print "rule 124"
        if len(p) == 2:
            label = p[1]
            if not self.symbol_table.is_present_in_scope(label.lower()):
                p[0] = label

    def p_iteration(self, p):
        '''iteration :
           | WHILE m condition
           | iter_part reverse_opt discrete_range
        '''
        if self.debug: print "rule 125"
        if len(p) == 4 and isinstance(p[1], str):
            curr_instr = self.three_add_code.instr_no
            p[0] = p[3]
            p[0].quad = p[2]
        elif len(p) == 4 and isinstance(p[1], Element):
            init_low, init_high = p[3]
            identifier = p[1].Name
            if p[2]:  # reverse is set
                init_val = init_high
                update_code = [identifier, identifier, 'int_-', 1]
                check_op = 'bgteq'
                limit = init_low
            else:
                init_val = init_low
                update_code = [identifier, identifier, 'int_+', 1]
                check_op = 'blteq'
                limit = init_high

            self.three_add_code.emit([identifier, init_val, ':=', None])
            curr_instr = self.three_add_code.instr_no
            self.three_add_code.emit(['goto', curr_instr+2, None, None])
            p[0] = Element()
            p[0].quad = curr_instr + 1
            self.three_add_code.emit(update_code)
            p[0].truelist = [curr_instr + 2]
            p[0].falselist = [curr_instr + 3]
            self.three_add_code.emit([check_op, identifier, limit, None])
            self.three_add_code.emit(['goto', None, None, None])


    def p_iter_part(self, p):
        '''iter_part : FOR IDENTIFIER IN
        '''
        if self.debug: print "rule 126"
        identifier = p[2]
        if not self.symbol_table.is_present_in_scope(identifier):
            p[0] = Element(Name=identifier, Type=Type(type_name='INTEGER'))
            p[0].Type.assign_width()
            p[0].Offset = self.symbol_table.get_width()
            self.symbol_table.add_width(p[0].Type.width)
            self.symbol_table.add_to_table(identifier, p[0], p.lineno)

    def p_reverse_opt(self, p):
        '''reverse_opt :
           | REVERSE
        '''
        if self.debug: print "rule 127"
        if len(p) == 2:
            p[0] = True
        else:
            p[0] = False

    def p_basic_loop(self, p):
        '''basic_loop : LOOP statement_s END LOOP
        '''
        if self.debug: print "rule 128"
        p[0] = deepcopy(p[2])
        curr_instr = self.three_add_code.instr_no
        p[0].nextlist += [curr_instr]
        self.three_add_code.emit(['goto', None, None, None])

    def p_id_opt(self, p):
        '''id_opt :
           | designator
        '''
        if self.debug: print "rule 129"
        if len(p) == 2:
            p[0] = p[1]

    def p_block(self, p):
        '''block : label_opt block_decl block_body END id_opt ';'
        '''
        if self.debug: print "rule 130"
        p[0] = deepcopy(p[3])

    def p_block_decl(self, p):
        '''block_decl :
           | DECLARE decl_part
        '''
        if self.debug: print "rule 131"

    def p_block_body(self, p):
        '''block_body : BEGIN handled_stmt_s
        '''
        if self.debug: print "rule 132"
        p[0] = p[2]

    def p_handled_stmt_s(self, p):
        '''handled_stmt_s : statement_s except_handler_part_opt
        '''
        if self.debug: print "rule 133"
        if p[2] is None:
            p[0] = p[1]

    def p_except_handler_part_opt(self, p):
        '''except_handler_part_opt :
           | except_handler_part
        '''
        if self.debug: print "rule 134"
        if len(p) == 1:
            p[0] = None
        else:
            self.p_error(p)
            print 'Exeption Handler not implemented'

    def p_exit_stmt(self, p):
        '''exit_stmt : EXIT name_opt when_opt ';'
        '''
        if self.debug: print "rule 135"

    def p_name_opt(self, p):
        '''name_opt :
           | name
        '''
        if self.debug: print "rule 136"

    def p_when_opt(self, p):
        '''when_opt :
           | WHEN condition
        '''
        if self.debug: print "rule 137"

    def p_return_stmt(self, p):
        '''return_stmt : RETURN ';'
           | RETURN expression ';'
        '''
        if self.debug: print "rule 138"

    def p_goto_stmt(self, p):
        '''goto_stmt : GOTO name ';'
        '''
        if self.debug: print "rule 139"

    def p_subprog_decl(self, p):
        '''subprog_decl : subprog_spec ';'
           | generic_subp_inst ';'
           | subprog_spec_is_push ABSTRACT ';'
        '''
        if self.debug: print "rule 140"

    def p_subprog_spec(self, p):
        '''subprog_spec : PROCEDURE compound_name formal_part_opt
           | FUNCTION designator formal_part_opt RETURN name
           | FUNCTION designator
        '''
        if self.debug: print "rule 141"
        if len(p) == 4:  # First case
            p[0] = Element()
            if p[2].Name is not None:
                if self.symbol_table.is_present_in_scope(p[2].Name):
                    other_one = self.symbol_table.lookup(p[2].Name)
                    # for overloading
                    if not self.symbol_table.is_same_procedure(
                            other_one.Type.params, p[3]):
                        p[0].Name = p[2].Name
                        proced_type = Type(type_name='PROCEDURE',
                                           params=p[3])
                        p[0].Type = proced_type
                    else:
                        self.p_error(p)
                        print p[2].Name + ' is already used in this scope'
                        p[0].Type = Type(type_name = 'ERROR')
                else:
                    p[0].Name = p[2].Name
                    proced_type = Type(type_name='PROCEDURE',
                                       params=p[3])
                    p[0].Type = proced_type
                    self.symbol_table.add_to_table(p[2].Name, p[0], p)
            else:
                self.p_error(p)
                print 'Invalid name for procedure'
                p[0].Type = Type(type_name = 'ERROR')
        elif len(p) == 6:  # Second case
            if p[2].Name is not None:
                if not self.symbol_table.is_present_in_scope(p[2].Name):
                    if p[5].Name is not None and p[5].Name.upper() in basic_types:
                        func_type = Type(type_name='FUNCTION',
                                         constituent_type=p[5].Name,
                                         params=p[3])
                        p[0] = Element(Name=p[2].Name, Type=func_type)
                        self.symbol_table.add_to_table(p[0].Name, p[0], p)
                    else:
                        self.p_error(p)
                        print 'Return types in basic types allowed only'
                else:
                    self.p_error(p)
                    print 'Function designator is already present in scope'
            else:
                self.p_error(p)
                print 'Invalid designator name'
        else:
            self.p_error(p)
            print 'Functions return type not specified'

    def p_designator(self, p):
        '''designator : compound_name
           | STRING
        '''
        if self.debug: print "rule 142"
        if isinstance(p[1], str):
            p[0] = Element(Name=p[1])
        else:
            p[0] = p[1]

    def p_formal_part_opt(self, p):
        '''formal_part_opt :
           | formal_part
        '''
        if self.debug: print "rule 143"
        if len(p) == 2:
            p[0] = p[1]
        else:
            p[0] = None

    def p_formal_part(self, p):
        '''formal_part : '(' param_s ')'
        '''
        if self.debug: print "rule 144"
        p[0] = p[2]

    def p_param_s(self, p):
        '''param_s : param
           | param_s ';' param
        '''
        if self.debug: print "rule 145"
        if len(p) == 2:
            p[0] = p[1]
        else:
            p[0] = p[1] + p[3]

    def p_param(self, p):
        '''param : def_id_s ':' mode mark init_opt
           | error
        '''
        if self.debug: print "rule 146"
        if len(p) == 6:
            p[0] = []
            if p[4].Name is not None:
                for ele in p[1]:
                    ele.Mode = p[3]
                    ele.Type = Type(type_name=p[4].Name.upper())
                    if p[5] is not None:
                        if ele.Type.type_name == p[5].Type.type_name:
                            ele.Value = p[5].Value
                            p[0] += [ele]
                        else:
                            print 'Error at line ' + str(p.lineno) \
                                + 'Init and Mark type do not match'
                    else:
                        p[0] += [ele]
            else:
                print 'Error at line ' + str(p.lineno) \
                    + ' Parameter Types not given'

    def p_mode(self, p):
        '''mode :
           | IN
           | OUT
           | IN OUT
           | ACCESS
        '''
        if self.debug: print "rule 147"
        if len(p) == 2:
            p[0] = p[1].upper()
        else:
            p[0] = 'InOut'

    def p_subprog_spec_is_push(self, p):
        '''subprog_spec_is_push : subprog_spec IS
        '''
        if self.debug: print "rule 148"
        p[0] = p[1]
        p[0].nextlist = []
        if self.symbol_table.parent_table is not None:
            p[0].nextlist = [self.three_add_code.instr_no]

        new_scope_table = SymbolTable(self.symbol_table)

        if p[1].Type.type_name == 'PROCEDURE':
            new_scope_table.setup_procedure(p[1].Type.params, p.lineno)
        else:
            new_scope_table.setup_function(p[1].Type.params, p.lineno)

        self.three_add_code.emit(['proc_label', p[1].Name, None, None])
        self.symbol_table = new_scope_table

    def p_subprog_body(self, p):
        '''subprog_body : subprog_spec_is_push decl_part block_body m END id_opt ';'
        '''
        #       0                      1            2          3    4  5    6     7
        if self.debug: print "rule 149"
        if p[6] is not None:
            if not p[6].Name == p[1].Name:
                self.p_error(p)
                print "Labels at the beginning and end do not match"
            else:
                p[0] = p[1]
                self.three_add_code.backpatch(p[3].nextlist, p[4])
                out_vars = ''
                for out_var in self.symbol_table.hash_table:
                    ele = self.symbol_table.lookup(out_var)
                    if ele.Mode == 'OUT':
                        out_vars += ' ' + out_var
                if out_vars == '':
                    self.three_add_code.emit(['return', None, None, None])
                else:
                    self.three_add_code.emit(['return', out_vars, None, None])

                if p[0] is not None:
                    self.three_add_code.backpatch(p[0].nextlist,
                                                  self.three_add_code.instr_no - 1)
                else:
                    print 'None statements detected'
        else:
            print 'Function/Procedure not ended with Function/Procedure name'
        self.symb_output += self.symbol_table.add_symb_output()
        child_width = self.symbol_table.get_width()
        self.symbol_table = self.symbol_table.parent_table
        self.symbol_table.add_width(child_width)

    def p_procedure_call(self, p):
        '''procedure_call : name ';'
        '''
        if self.debug: print "rule 150"
        p[0] = p[1]
        p[0].nextlist = []

    def p_pkg_decl(self, p):
        '''pkg_decl : pkg_spec ';'
           | generic_pkg_inst ';'
        '''
        if self.debug: print "rule 151"

    def p_pkg_spec(self, p):
        '''pkg_spec : PACKAGE compound_name IS decl_item_s private_part END c_id_opt
        '''
        if self.debug: print "rule 152"

    def p_private_part(self, p):
        '''private_part :
           | PRIVATE decl_item_s
        '''
        if self.debug: print "rule 153"

    def p_c_id_opt(self, p):
        '''c_id_opt :
           | compound_name
        '''
        if self.debug: print "rule 154"

    def p_pkg_body(self, p):
        '''pkg_body : PACKAGE BODY compound_name IS decl_part body_opt END c_id_opt ';'
        '''
        if self.debug: print "rule 155"

    def p_body_opt(self, p):
        '''body_opt :
           | block_body
        '''
        if self.debug: print "rule 156"

    def p_private_type(self, p):
        '''private_type : tagged_opt limited_opt PRIVATE
        '''
        if self.debug: print "rule 157"

    def p_limited_opt(self, p):
        '''limited_opt :
           | LIMITED
        '''
        if self.debug: print "rule 158"

    def p_use_clause(self, p):
        '''use_clause : USE name_s ';'
           | USE TYPE name_s ';'
        '''
        if self.debug: print "rule 159"

    def p_name_s(self, p):
        '''name_s : name
           | name_s ',' name
        '''
        if self.debug: print "rule 160"
        if len(p) == 2:
            p[0] = [p[1]]
        else:
            p[0] = p[1] + [p[3]]

    def p_rename_decl(self, p):
        '''rename_decl : def_id_s ':' object_qualifier_opt subtype_ind renames ';'
           | def_id_s ':' EXCEPTION renames ';'
           | rename_unit
        '''
        if self.debug: print "rule 161"

    def p_rename_unit(self, p):
        '''rename_unit : PACKAGE compound_name renames ';'
           | subprog_spec renames ';'
           | generic_formal_part PACKAGE compound_name renames ';'
           | generic_formal_part subprog_spec renames ';'
        '''
        if self.debug: print "rule 162"

    def p_renames(self, p):
        '''renames : RENAMES name
        '''
        if self.debug: print "rule 163"

    def p_task_decl(self, p):
        '''task_decl : task_spec ';'
        '''
        if self.debug: print "rule 164"

    def p_task_spec(self, p):
        '''task_spec : TASK simple_name task_def
           | TASK TYPE simple_name discrim_part_opt task_def
        '''
        if self.debug: print "rule 165"

    def p_task_def(self, p):
        '''task_def :
           | IS entry_decl_s rep_spec_s task_private_opt END id_opt
        '''
        if self.debug: print "rule 166"

    def p_task_private_opt(self, p):
        '''task_private_opt :
           | PRIVATE entry_decl_s rep_spec_s
        '''
        if self.debug: print "rule 167"

    def p_task_body(self, p):
        '''task_body : TASK BODY simple_name IS decl_part block_body END id_opt ';'
        '''
        if self.debug: print "rule 168"

    def p_prot_decl(self, p):
        '''prot_decl : prot_spec ';'
        '''
        if self.debug: print "rule 169"

    def p_prot_spec(self, p):
        '''prot_spec : PROTECTED IDENTIFIER prot_def
           | PROTECTED TYPE simple_name discrim_part_opt prot_def
        '''
        if self.debug: print "rule 170"

    def p_prot_def(self, p):
        '''prot_def : IS prot_op_decl_s prot_private_opt END id_opt
        '''
        if self.debug: print "rule 171"

    def p_prot_private_opt(self, p):
        '''prot_private_opt :
           | PRIVATE prot_elem_decl_s
        '''
        if self.debug: print "rule 172"

    def p_prot_op_decl_s(self, p):
        '''prot_op_decl_s :
           | prot_op_decl_s prot_op_decl
        '''
        if self.debug: print "rule 173"

    def p_prot_op_decl(self, p):
        '''prot_op_decl : entry_decl
           | subprog_spec ';'
           | rep_spec
           | pragma
        '''
        if self.debug: print "rule 174"

    def p_prot_elem_decl_s(self, p):
        '''prot_elem_decl_s :
           | prot_elem_decl_s prot_elem_decl
        '''
        if self.debug: print "rule 175"

    def p_prot_elem_decl(self, p):
        '''prot_elem_decl : prot_op_decl
        | comp_decl
        '''
        if self.debug: print "rule 176"

    def p_prot_body(self, p):
        '''prot_body : PROTECTED BODY simple_name IS prot_op_body_s END id_opt ';'
        '''
        if self.debug: print "rule 177"

    def p_prot_op_body_s(self, p):
        '''prot_op_body_s : pragma_s
           | prot_op_body_s prot_op_body pragma_s
        '''
        if self.debug: print "rule 178"

    def p_prot_op_body(self, p):
        '''prot_op_body : entry_body
           | subprog_body
           | subprog_spec ';'
        '''
        if self.debug: print "rule 179"

    def p_entry_decl_s(self, p):
        '''entry_decl_s : pragma_s
           | entry_decl_s entry_decl pragma_s
        '''
        if self.debug: print "rule 179"

    def p_entry_decl(self, p):
        '''entry_decl : ENTRY IDENTIFIER formal_part_opt ';'
           | ENTRY IDENTIFIER '(' discrete_range ')' formal_part_opt ';'
        '''
        if self.debug: print "rule 180"

    def p_entry_body(self, p):
        '''entry_body : ENTRY IDENTIFIER formal_part_opt WHEN condition entry_body_part
           | ENTRY IDENTIFIER '(' iter_part discrete_range ')' formal_part_opt WHEN condition entry_body_part
        '''
        if self.debug: print "rule 181"

    def p_entry_body_part(self, p):
        '''entry_body_part : ';'
           | IS decl_part block_body END id_opt ';'
        '''
        if self.debug: print "rule 182"

    def p_rep_spec_s(self, p):
        '''rep_spec_s :
           | rep_spec_s rep_spec pragma_s
        '''
        if self.debug: print "rule 183"

    def p_entry_call(self, p):
        '''entry_call : procedure_call
        '''
        if self.debug: print "rule 184"

    def p_accept_stmt(self, p):
        '''accept_stmt : accept_hdr ';'
           | accept_hdr DO handled_stmt_s END id_opt ';'
        '''
        if self.debug: print "rule 185"

    def p_accept_hdr(self, p):
        '''accept_hdr : ACCEPT entry_name formal_part_opt
        '''
        if self.debug: print "rule 186"

    def p_entry_name(self, p):
        '''entry_name : simple_name
           | entry_name '(' expression ')'
        '''
        if self.debug: print "rule 187"

    def p_delay_stmt(self, p):
        '''delay_stmt : DELAY expression ';'
           | DELAY UNTIL expression ';'
        '''
        if self.debug: print "rule 188"

    def p_select_stmt(self, p):
        '''select_stmt : select_wait
           | async_select
           | timed_entry_call
           | cond_entry_call
        '''
        if self.debug: print "rule 189"

    def p_select_wait(self, p):
        '''select_wait : SELECT guarded_select_alt or_select else_opt  END SELECT ';'
        '''
        if self.debug: print "rule 190"

    def p_guarded_select_alt(self, p):
        '''guarded_select_alt : select_alt
           | WHEN condition ARROW select_alt
        '''
        if self.debug: print "rule 191"

    def p_or_select(self, p):
        '''or_select :
           | or_select OR guarded_select_alt
        '''
        if self.debug: print "rule 192"

    def p_select_alt(self, p):
        '''select_alt : accept_stmt stmts_opt
           | delay_stmt stmts_opt
           | TERMINATE ';'
        '''
        if self.debug: print "rule 193"

    def p_delay_or_entry_alt(self, p):
        '''delay_or_entry_alt : delay_stmt stmts_opt
           | entry_call stmts_opt
        '''
        if self.debug: print "rule 194"

    def p_async_select(self, p):
        '''async_select : SELECT delay_or_entry_alt THEN ABORT statement_s END SELECT ';'
        '''
        if self.debug: print "rule 195"

    def p_timed_entry_call(self, p):
        '''timed_entry_call : SELECT entry_call stmts_opt OR delay_stmt stmts_opt END SELECT ';'
        '''
        if self.debug: print "rule 196"

    def p_cond_entry_call(self, p):
        '''cond_entry_call : SELECT entry_call stmts_opt ELSE statement_s END SELECT ';'
        '''
        if self.debug: print "rule 197"

    def p_stmts_opt(self, p):
        '''stmts_opt :
           | statement_s
        '''
        if self.debug: print "rule 198"

    def p_abort_stmt(self, p):
        '''abort_stmt : ABORT name_s ';'
        '''
        if self.debug: print "rule 199"

    def p_compilation(self, p):
        '''compilation :
           | compilation comp_unit
           | pragma pragma_s
        '''
        if self.debug: print "rule 200"

    def p_comp_unit(self, p):
        '''comp_unit : context_spec private_opt unit pragma_s
           | private_opt unit pragma_s
        '''
        if self.debug: print "rule 201"

    def p_private_opt(self, p):
        '''private_opt :
           | PRIVATE
        '''
        if self.debug: print "rule 202"
        if len(p) == 1:
            p[0] = False
        else:
            True

    def p_context_spec(self, p):
        '''context_spec : with_clause use_clause_opt
           | context_spec with_clause use_clause_opt
           | context_spec pragma
        '''
        if self.debug: print "rule 203"

    def p_with_clause(self, p):
        '''with_clause : WITH c_name_list ';'
        '''
        if self.debug: print "rule 204"
        p[0] = p[1]

    def p_use_clause_opt(self, p):
        '''use_clause_opt :
           | use_clause_opt use_clause
        '''
        if self.debug: print "rule 205"

    def p_unit(self, p):
        '''unit : pkg_decl
           | pkg_body
           | subprog_decl
           | subprog_body
           | subunit
           | generic_decl
           | rename_unit
        '''
        if self.debug: print "rule 206"

    def p_subunit(self, p):
        '''subunit : SEPARATE '(' compound_name ')' subunit_body
        '''
        if self.debug: print "rule 207"

    def p_subunit_body(self, p):
        '''subunit_body : subprog_body
           | pkg_body
           | task_body
           | prot_body
        '''
        if self.debug: print "rule 208"
        p[0] = p[1]

    def p_body_stub(self, p):
        '''body_stub : TASK BODY simple_name IS SEPARATE ';'
           | PACKAGE BODY compound_name IS SEPARATE ';'
           | subprog_spec IS SEPARATE ';'
           | PROTECTED BODY simple_name IS SEPARATE ';'
        '''
        if self.debug: print "rule 209"

    def p_exception_decl(self, p):
        '''exception_decl : def_id_s ':' EXCEPTION ';'
        '''
        if self.debug: print "rule 210"

    def p_except_handler_part(self, p):
        '''except_handler_part : EXCEPTION exception_handler
           | except_handler_part exception_handler
        '''
        if self.debug: print "rule 211"

    def p_exception_handler(self, p):
        '''exception_handler : WHEN except_choice_s ARROW statement_s
           | WHEN IDENTIFIER ':' except_choice_s ARROW statement_s
        '''
        if self.debug: print "rule 212"

    def p_except_choice_s(self, p):
        '''except_choice_s : except_choice
           | except_choice_s '|' except_choice
        '''
        if self.debug: print "rule 213"

    def p_except_choice(self, p):
        '''except_choice : name
           | OTHERS
        '''
        if self.debug: print "rule 214"

    def p_raise_stmt(self, p):
        '''raise_stmt : RAISE name_opt ';'
        '''
        if self.debug: print "rule 215"

    def p_requeue_stmt(self, p):
        '''requeue_stmt : REQUEUE name ';'
           | REQUEUE name WITH ABORT ';'
        '''
        if self.debug: print "rule 216"

    def p_generic_decl(self, p):
        '''generic_decl : generic_formal_part subprog_spec ';'
           | generic_formal_part pkg_spec ';'
        '''
        if self.debug: print "rule 217"

    def p_generic_formal_part(self, p):
        '''generic_formal_part : GENERIC
           | generic_formal_part generic_formal
        '''
        if self.debug: print "rule 218"

    def p_generic_formal(self, p):
        '''generic_formal : param ';'
           | TYPE simple_name generic_discrim_part_opt IS generic_type_def ';'
           | WITH PROCEDURE simple_name formal_part_opt subp_default ';'
           | WITH FUNCTION designator formal_part_opt RETURN name subp_default ';'
           | WITH PACKAGE simple_name IS NEW name '(' BOX ')' ';'
           | WITH PACKAGE simple_name IS NEW name ';'
           | use_clause
        '''
        if self.debug: print "rule 219"

    def p_generic_discrim_part_opt(self, p):
        '''generic_discrim_part_opt :
           | discrim_part
           | '(' BOX ')'
        '''
        if self.debug: print "rule 220"

    def p_subp_default(self, p):
        '''subp_default :
           | IS name
           | IS BOX
        '''
        if self.debug: print "rule 221"

    def p_generic_type_def(self, p):
        '''generic_type_def : '(' BOX ')'
           | RANGE BOX
           | MOD BOX
           | DELTA BOX
           | DELTA BOX DIGITS BOX
           | DIGITS BOX
           | array_type
           | access_type
           | private_type
           | generic_derived_type
        '''
        if self.debug: print "rule 222"

    def p_generic_derived_type(self, p):
        '''generic_derived_type : NEW subtype_ind
           | NEW subtype_ind WITH PRIVATE
           | ABSTRACT NEW subtype_ind WITH PRIVATE
        '''
        if self.debug: print "rule 223"

    def p_generic_subp_inst(self, p):
        '''generic_subp_inst : subprog_spec IS generic_inst
        '''
        if self.debug: print "rule 224"

    def p_generic_pkg_inst(self, p):
        '''generic_pkg_inst : PACKAGE compound_name IS generic_inst
        '''
        if self.debug: print "rule 225"

    def p_generic_inst(self, p):
        '''generic_inst : NEW name
        '''
        if self.debug: print "rule 226"

    def p_rep_spec(self, p):
        '''rep_spec : attrib_def
           | record_type_spec
           | address_spec
        '''
        if self.debug: print "rule 227"

    def p_attrib_def(self, p):
        '''attrib_def : FOR mark USE expression ';'
        '''
        if self.debug: print "rule 228"

    def p_record_type_spec(self, p):
        '''record_type_spec : FOR mark USE RECORD align_opt comp_loc_s END RECORD ';'
        '''
        if self.debug: print "rule 229"

    def p_align_opt(self, p):
        '''align_opt :
           | AT MOD expression ';'
        '''
        if self.debug: print "rule 230"

    def p_comp_loc_s(self, p):
        '''comp_loc_s :
           | comp_loc_s mark AT expression RANGE range ';'
        '''
        if self.debug: print "rule 231"

    def p_address_spec(self, p):
        '''address_spec : FOR mark USE AT expression ';'
        '''
        if self.debug: print "rule 232"

    def p_code_stmt(self, p):
        '''code_stmt : qualified ';'
        '''
        if self.debug: print "rule 233"

    def p_error(self, p):
        try:
            self.successful = False
            print "Error " + " found at line " \
                + str(p.lineno)
            parser.errok()
        except:
            pass
