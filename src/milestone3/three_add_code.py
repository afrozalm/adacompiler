from copy import deepcopy
from elements import Element


class ThreeAddCode(object):
    def __init__(self, symbol_table):
        self.instr_no = 0
        self.code_list = []
        self.temp_variable_number = 0

    def get_temp_name(self):
        name = '_t' + str(self.temp_variable_number)
        self.temp_variable_number += 1
        return name

    def new_temp(self, Type, symbol_table):
        name = self.get_temp_name()
        temp_offset = symbol_table.get_width()
        temp_type = deepcopy(Type)
        temp_type.assign_width()
        temp = Element(Name=name, Offset=temp_offset, Type=temp_type)
        symbol_table.add_width(temp_type.width)
        return temp

    def emit(self, code):
        # print 'emiting ', code
        self.code_list += [[self.instr_no] + code]
        self.instr_no += 1

    def backpatch(self, List, jump_addr):
        for lineno in List:
            idx = 0
            while self.code_list[lineno][idx] is not None:
                idx += 1
            self.code_list[lineno][idx] = jump_addr

    def display_code(self):
        print "========================================="
        print '      Displaying three-address-code'
        print "========================================="
        for code in self.code_list:
            lineno, lhs, op1, op, op2 = code
            if op == ':=':
                print lineno, '\t', lhs, '<-', op1
            elif lhs in ['goto', 'proc_label']:
                print lineno, '\t', lhs + ': ', op1
            elif lhs in ['return']:
                if op1 is None:
                    op1 = ''
                print lineno, '\t', lhs, op1
            elif lhs in ['if']:
                print lineno, '\t', lhs, op1, op, op2
            else:
                print lineno, '\t', lhs, '<-', op1, op, op2
        print "========================================="
