# ADA Comiler #
A compiler that converts an Ada program to x86 Assembly language, implemented in Python using PLY library. 

### Following representations of an Ada program are supported: ###
 * Parse Tree
 * Abstract Syntax Tree
 * 3 Address Code
 * x86 Assembly Code

### Language Constructs Handled
 * Native Types (Integer, Boolean, Character)
 * Input and Output (only for character strings and integers)
 * Basic algebraic expressions and relations
 * Multi-dimensional arrays
 * If-elsif-else statement
 * Loops: both for and while (with the option to reverse the iterator)
 * Procedures without recursion

### Functionalities Yet to be Provided
 * Workability with Floats
 * Pointer Manipulation
 * Recursion
 * Structs and user-defined types

### Miscellaneous
 Though very little optimizations have been considered for code generation, we do optimize the process of code generation. This is done by using hash tables in the symbol tables and backpatcing while 3AC generation.

### Instructions to run the code: ###
 * ```cd src/milestone4```
 * ```./main.py filename_1 filename_2 [more filenames if needed]```